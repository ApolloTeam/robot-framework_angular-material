'esversion: 6';

var gulp = require('gulp');

// Bower 
var bower = require('gulp-bower');
var sourcemaps = require('gulp-sourcemaps');
var minify = require('gulp-minify');
var cleanCSS = require('gulp-clean-css');
var htmlmin = require('gulp-htmlmin');
var strip = require('gulp-strip-comments');
var templateCache = require('gulp-angular-templatecache');
var gulp_tslint = require('gulp-tslint');
var sass = require('gulp-sass');
var rename = require('gulp-rename');

// Detecta los cambios de los archivos de entrada y los archivos de salida.
var changed = require('gulp-changed');

// Typescript
var ts = require('gulp-typescript');
var tsProject = ts.createProject('tsconfig.json', {
    typescript: require('typescript')
});

var exec = require('child_process').exec;

var clean = require('gulp-clean');
var del = require('del');
var filesExist = require('files-exist');

var ngAnnotate = require('gulp-ng-annotate');

// Utilitarios para acceder a los archivos del disco.
var fs = require('fs');
var path = require('path');

// var globfs = require('glob-fs')({ gitignore: false });
var glob = require('glob');
var replace = require('gulp-replace-path');

//
var gutil = require('gulp-util');

// Utilitarios para trabajar con los stream.
var through = require('through2');
var merge = require('merge-stream');

// Zip
var zip = require('gulp-zip');

// Configuraciones

// Configuración para anotar los archivos de los modulos.
var ngAnnotateOptions = {
    remove: false,
    add: true,
    single_quotes: true
};

// Configuración para validar si los archivos existen. 
var filesExistConfig = {
    checkGlobs: true,
    throwOnMissing: false
};

// Paths de los archivos.
var paths = {
    views: ['src/**/*.html',
        'src/**/*.css',
        'src/**/*.svg',
        'src/**/*.png',
        'src/**/*.jpg',
        'src/**/*.gif',
        'src/**/*.json',
        'src/**/*.woff'
    ],
    images: [
        'src/**/*.svg',
        'src/**/*.png',
        'src/**/*.jpg',
        'src/**/*.gif'
    ],
    dest: 'www',
    envfiles: ['www/core/http-resolver-*.service.js*',
        'www/app/environment/environment-*.module.js*']
};

// Configuración para el minificado de html.
var htmlminOptions = {
    collapseWhitespace: true
};

/**
 * Corre tslint en todos los archivos .ts.
 * Source: https://marketplace.visualstudio.com/items?itemName=eg2.tslint (al final).
 */
gulp.task('tslint-ide', function () {
    gulp.src(['src/**/*.ts'])
        .pipe(gulp_tslint({
            // NOTE: Para que se vean en la solapa PROBLEMS de VSCode, requiere formater "prose" 
            // y una tarea en VSCode que mande la salida a PROBLEMS.
            // Otros formaters: https://palantir.github.io/tslint/formatters/.
            formatter: "prose"
        }))
        .pipe(gulp_tslint.report({
            emitError: false
        }));
});

/**
 * bower
 * Actualiza las dependencias de bower.
 */
gulp.task('bower', function () {
    return bower({
        cmd: 'update'
    });
});

/**
 * minify-css
 * Minifica los fuentes de los css y los copia a la carpeta www.
 * Ejecuta: sass. 
 */
gulp.task('minify-css', ['sass'], function () {
    return gulp.src(['www/**/*.css', '!www/vendor/**/*.css'])
        .pipe(cleanCSS({
            compatibility: 'ie8'
        }))
        .pipe(gulp.dest(paths.dest));
});

/**
 * default
 * Ejecuta: debug
 * Tarea predeterminada.
 */
gulp.task('default', ['mock', 'bower-installer']);

/**
 * debug
 * Ejecuta: sass, scripts-debug, scripts-js, views-debug, bower-installer
 */
gulp.task('internal-debug', ['sass', 'scripts-debug', 'scripts-js', 'views-debug', 'bower-installer']);


gulp.task('debug', ['internal-debug'], cleanEnvironmentFiles);

/**
 * debug-fast
 * Ejecuta: sass, scripts-debug, scripts-js, views-debug
 * Se utiliza con el watch.
 */
gulp.task('debug-fast', ['internal-debug-fast'], cleanEnvironmentFiles);

gulp.task('internal-debug-fast', ['sass', 'scripts-debug', 'scripts-js', 'views-debug']);

gulp.task('mock', ['override-mock-files'], cleanEnvironmentFiles);


function cleanEnvironmentFiles() {
    return gulp.src(paths.envfiles)
        .pipe(clean());
}

gulp.task('override-mock-files', ['clean-mock-files'], function () {
    return merge(gulp.src(['www/core/http-resolver-delay.service.js'])
        .pipe(rename('http-resolver.service.js'))
        .pipe(gulp.dest('www/core')),
        gulp.src(['www/core/http-resolver-delay.service.js.map'])
            .pipe(rename('http-resolver.service.js.map'))
            .pipe(gulp.dest('www/core')),
        gulp.src(['www/app/environment/environment-mock.module.js'])
            .pipe(rename('environment.module.js'))
            .pipe(gulp.dest('www/app/environment')),
        gulp.src(['www/app/environment/environment-mock.module.js.map'])
            .pipe(rename('environment.module.js.map'))
            .pipe(gulp.dest('www/app/environment')));
});


gulp.task('clean-mock-files', ['internal-debug-fast'], function () {
    return gulp.src(['www/core/http-resolver.service.js',
        'www/core/http-resolver.service.js.map',
        'www/app/environment/environment.module.js',
        'www/app/environment/environment.module.js.map'])
        .pipe(clean());
});

/**
 * release
 * Ejecuta: scripts-release, scripts-js, views-release, bower-installer-release
 * Limpia los archivos que no se utilizan en la versión de release.
 */
gulp.task('release', ['scripts-release', 'scripts-js', 'views-release', 'bower-installer-release'], function () {
    return gulp.src([
        'www/**/*.i18n.*.json',
        'www/**/*model.js',
        'www/**/*viewData.js',
        'www/**/*.entity.js',
        'www/**/*.js.map',
        'www/core/entities/http-promise-cancelable.js',
        'www/core/entities/promise-cancelable.js',
        'www/vendor/angular-mocks/*',
        'www/vendor/angular-mocks',
        'www/**/i18n/',
        'www/**/*.mock.*.js'
    ])
        .pipe(clean());
});

/**
 * views-debug
 * Copia las vistas a la carpeta www.
 */
gulp.task('views-debug', function () {
    return gulp.src(paths.views)
        .pipe(changed(paths.dest))
        .pipe(gulp.dest(paths.dest));
});

/**
 * views-release
 * Ejecuta: templates-release, minify-index-html, minify-css
 * Copia las imagenes a la carpeta www.
 */
gulp.task('views-release', ['templates-release', 'minify-index-html', 'minify-css'], function () {
    return gulp.src(paths.images)
        .pipe(changed(paths.dest))
        .pipe(gulp.dest(paths.dest));
});

/**
 * minify-html
 * Minifica los archivos .html, remueve los comentarios.
 */
gulp.task('minify-html', function () {
    return gulp.src('src/**/*.html')
        .pipe(changed(paths.dest))
        .pipe(strip())
        .pipe(htmlmin(htmlminOptions))
        .pipe(gulp.dest(paths.dest));
});

/**
 * minify-index-html  
 * Minifica el index.html, remueve los comentarios.
 */
gulp.task('minify-index-html', function () {
    return gulp.src('src/**/index.html')
        .pipe(changed(paths.dest))
        .pipe(strip())
        .pipe(htmlmin(htmlminOptions))
        .pipe(gulp.dest(paths.dest));
});

/**
 * scripts-js
 * Copia los archivos del tipo JS de fuentes a la carpeta www.
 */
gulp.task('scripts-js', function () {
    return gulp.src('src/**/*.js')
        .pipe(gulp.dest(paths.dest));
});

/**
 * annotate-scripts
 * Ejecuta: compile-scripts
 * Anota los archivos JS resultantes que sean modulos 
 */
gulp.task('annotate-scripts', ['compile-scripts'], function () {
    return gulp.src([
        '!www/vendor/**/*.js',
        'www/**/*module.js'
    ])
        .pipe(ngAnnotate(ngAnnotateOptions))
        .pipe(gulp.dest(paths.dest));
});

/**
 * compile-scripts
 * Compila los fuentes de los archivos TS a JS 
 * e incluye los .map para facilitar el debug de la aplicación.
 */
gulp.task('compile-scripts', function () {
    return gulp.src([
        'src/**/*.ts',
        '!src/**/*.templates.ts',
        '!src/**/*.i18n.ts'
    ])
        .pipe(sourcemaps.init())
        .pipe(tsProject())
        .pipe(sourcemaps.write('/', {
            includeContent: true
        }))
        .pipe(gulp.dest(paths.dest));
});

/**
 * scripts-release
 * Ejecuta: annotate-scripts.
 * Compila los TS como JS y los anota para que se injecten 
 * correctamente las dependencias en tiempo de ejecución.
 * Minifica los archivos JS resultantes.
 */
gulp.task('scripts-release', ['annotate-scripts'], function () {
    return gulp.src([
        '!www/vendor/**/*.js',
        'www/**/*.js'
    ])
        .pipe(minify({
            ext: {
                src: '-debug.js',
                min: '.js'
            },
            noSource: true,
            ignoreFiles: ['-min.js'],
            mangle: false
        }))
        .pipe(gulp.dest(paths.dest));
});

/**
 * scripts-debug
 * Ejecuta: annotate-scripts.
 * Compila los TS como JS y los anota para que se injecten 
 * correctamente las dependencias en tiempo de ejecución.
 */
gulp.task('scripts-debug', ['annotate-scripts'], function () {
    return gulp.src([
        'src/**/*.templates.ts',
        'src/**/*.i18n.ts'
    ])
        .pipe(sourcemaps.init())
        .pipe(tsProject())
        .pipe(gulp.dest(paths.dest));
});

/**
 * watch
 * Monitorea los cambios en los archivos src y ejecuta la tarea que los compila.
 */
gulp.task('watch', function () {
    gulp.watch('src/**/*.*', ['debug-fast']);
});

gulp.task('watch-mock', function () {
    gulp.watch('src/**/*.*', ['mock']);
});

/**
 * templates-release
 * Ejecuta: templates-release-html, templates-release-json
 */
gulp.task('templates-release', ['templates-release-html', 'templates-release-json']);

/**
 * templates-release-html
 * Compila los archivos .html y los minifica en un unico archivo 
 * JS que los agrega al template cache de angularjs.
 * Mejora la performance de carga de las aplicaciones. 
 * Se penaliza la carga inicial cargando los templates.
 */
gulp.task('templates-release-html', function () {
    // Busca las carpetas que contengan .html.
    var folders = getFolders('src/**/*.html');
    return folders.map(function (folder) {
        var name = folder.split('/')[1];
        var filename = (folder + '/' + name + '.templates.js').replace('src/', '');
        return gulp.src([folder + '/**/*.html', '!app/index.html'])
            .pipe(strip())
            .pipe(htmlmin(htmlminOptions))
            .pipe(
            templateCache({
                filename: filename,
                module: name + '.templates',
                moduleSystem: 'IIFE',
                standalone: true,
                templateHeader: 'angular.module(\'<%= module %>\' <%= standalone %>).run([\'$templateCache\', function($templateCache) { console.log(\'' + name + '.templates\');',
                templateBody: '$templateCache.put(\'' + name + '/<%= url %>\',\'<%= contents %>\');'
            })
            )
            .pipe(minify({
                ext: {
                    src: '-debug.js',
                    min: '.js'
                },
                noSource: true,
                ignoreFiles: ['-min.js'],
                mangle: false
            }))
            .pipe(gulp.dest(paths.dest));
    });
});

/**
 * templates-release-json
 * Empaqueta los json de localización y los minifica en un unico archivo 
 * JS que los agrega al template cache de angularjs.
 * Mejora la performance de carga de las aplicaciones. 
 * Se penaliza la carga inicial cargando los templates.
 */
gulp.task('templates-release-json', function () {
    // Busca las carpetas que contengan .json.
    var folders = getFolders('src/**/*.json');
    return folders.map(function (folder) {
        var name = folder.split('/')[1];
        var filename = (folder + '/' + name + '.i18n.js').replace('src/', '');
        return gulp.src([folder + '/**/*.json'])
            .pipe(strip())
            .pipe(htmlmin(htmlminOptions))
            .pipe(
            templateCache({
                filename: filename,
                module: name + '.i18n',
                moduleSystem: 'IIFE',
                standalone: true,
                templateHeader: 'angular.module(\'<%= module %>\'<%= standalone %>).run([\'$cacheFactory\', function($cacheFactory) { console.log(\'' + name + '.i18n\'); var httpCache = $cacheFactory.get(\'$http\');',
                templateBody: 'httpCache.put(\'' + name + '/<%= url %>\', [200, \'<%= contents %>\', {}, \'OK\']); ',
                templateFooter: '}]);'
            })
            ).pipe(minify({
                ext: {
                    src: '-debug.js',
                    min: '.js'
                },
                noSource: true,
                ignoreFiles: ['-min.js'],
                mangle: false
            }))
            .pipe(gulp.dest(paths.dest));
    });
});

/**
 * clean
 * Borra todo el contenido de la carpeta wwww.
 */
gulp.task('clean', function () {
    return gulp.src('www/*')
        .pipe(clean());
});

/**
 * bower-installer
 * Ejecuta la tarea bower-installer que copia las dependencias de bower a vendor.
 */
gulp.task('bower-installer', ['manual-bower-installer'], function (done) {
    exec('bower-installer', function (err, stdout, stderr) {
        console.error(stderr);
        del(filesExist([], filesExistConfig));
        done(err);
    });
});

/**
 * bower-installer-release
 * Ejecuta: bower-installer.
 * Ejecuta la tarea bower-installer y minifica los archivos de vendor.
 */
gulp.task('bower-installer-release', ['bower-installer'], function () {
    var minjs = gulp.src(['www/vendor/**/*.js'])
        .pipe(minify({
            ext: {
                src: '-debug.js',
                min: '.js'
            },
            noSource: true,
            ignoreFiles: ['-min.js'],
            mangle: false
        }))
        .pipe(gulp.dest('www/vendor'));

    var mincss = gulp.src(['www/vendor/**/*.css'])
        .pipe(cleanCSS({
            compatibility: 'ie8'
        }))
        .pipe(gulp.dest('www/vendor'));

    return merge(minjs, mincss);
});

/**
 * manual-bower-installer
 * Copia las dependencias a la carpeta vendor.
 */
gulp.task('manual-bower-installer', function () {
    const uiRouterFiles = ['node_modules/@uirouter/angularjs/release/angular-ui-router.js',
        'node_modules/@uirouter/angularjs/release/angular-ui-router.js.map'];
    return gulp.src(uiRouterFiles)
        .pipe(gulp.dest('www/vendor/@uirouter'));
});

/**
 * sass
 * Compila los sass a css.
 */
gulp.task('sass', function (done) {
    gulp.src(['src/**/*.scss'])
        .pipe(sass())
        .on('error', sass.logError)
        .pipe(gulp.dest(paths.dest))
        .on('end', done);
});

/**
 * package
 * Ejecuta release
 * Empaqueta la aplicación en .zip.
 */
gulp.task('package', ['release'], function (cb) {
    var dt = new Date();
    var date = dt.toISOString().substr(0, 10).replace(/-/g, '') + '001';
    var fileName = 'www/' + date + '.log';
    var fd = fs.openSync(fileName, 'w+');
    fs.writeSync(fd, date);

    return gulp.src('www/**/*')
        .pipe(zip('AuthWeb.zip'))
        // .pipe(replace(/www/, ''))
        .pipe(gulp.dest('packages'));
});

/**
 * Devuelve la lista de los directorios que contienen archivos que coinciden con la expresion glob dada.
 * Se toman los directorios apartir de src.
 * Se toma solo el primer niivel.
 * Ej: SI: src/app
 *     SI: src/api
 *     SI: src/core
 *     NO: src/app/auth/reset-password
 * @param {*} dir 
 */
function getFolders(dir) {
    var data = glob.sync(dir).map(function (dirPath) {
        return path.dirname(dirPath);
    }).filter(function (dirPath) {
        return dirPath.split('/').length >= 2;
    }).map(function (dirPath) {
        var aux = dirPath.split('/');
        return aux.slice(0, 2).join('/');
    });

    var ret = [...new Set(data)];
    return ret;
}

/**
 * Imprime la etiqueta y el nombre de archivo en la salida de gulp.
 * @param {*} label Etiqueta a mostrar en la salida.
 */
function logFile(label) {
    function log(file, enc, cb) {
        gutil.log(label, ":", file.path);
        cb(null, file);
    }

    return through.obj(log);
}