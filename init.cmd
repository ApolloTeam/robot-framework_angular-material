@ECHO OFF
TITLE Inicializa el proyecto
ECHO Descargando dependencias npm
npm i
ECHO Descargando dependencias bower
bower install
ECHO Descargando dependencias instalando dependencias bower
bower-installer
ECHO Compilando el proyecto
gulp
ECHO Iniciando el servidor local
server