'use strict';

import angular from 'angular';
import 'angular-translate';
import { IStatusMessage } from './../core/entities/status-message.entity';
import { ApiErrorCodes } from './../api/commons/api-error-codes';
import { ErrorCodes } from '../core/entities/error-codes';
import { IErrorMessage } from '../core/entities/error-message.entity';
import { I18nService } from './i18n.service';
import { Injectable } from 'angular-ts-decorators';

/**
 * Manejador de errores no controlados.
 * @class ErrorHandlerService
 * @implements {IErrorHandlerService}
 */
@Injectable()
export class ErrorHandlerService {

    /**
     * Declaración de dependencias.
     * @static
     * @memberOf ErrorHandlerService
     */
    public static $inject = [
        '$log',
        '$q',
        '$translate',
        '$mdDialog',
        I18nService.name
    ];

    /**
     * Creates an instance of ErrorHandlerService.
     * @param {angular.ILogService} $log Servicio de logs.
     * @param {angular.IQService} $q Servicio para administrar el manejo de promesas.
     * @param {angular.translate.ITranslateService} $translate Servicio para localización de textos.
     * @param {angular.material.IDialogService} $mdDialog Servicio de dialog.
     * @param {II18nService} i18nService Realiza la configuracion para la localización de texto.
     * @memberOf ErrorHandlerService
     */
    constructor(
        private $log: angular.ILogService,
        private $q: angular.IQService,
        private $translate: angular.translate.ITranslateService,
        private $mdDialog: angular.material.IDialogService,
        private i18nService: I18nService) {

        this.$log.debug(`${ErrorHandlerService.name}::ctor`);

        // Asigna el nombre del módulo para la localización. 
        this.i18nService.configView('core');
    }

    /**
     * Procesa el error especificado.
     * @param {IStatusMessage} statusMessage Mensaje de error. 
     * @returns {IErrorMessage} Mensaje a mostrar al usuario.
     * @memberOf ErrorHandlerService 
     */
    public process(statusMessage: IStatusMessage): IErrorMessage {
        let msg: IErrorMessage;
    
        // tslint:disable-next-line:no-any
        switch (<any>statusMessage.errorCode) {
            case ErrorCodes.ErrUserCancel:
            case 'ERR.GP':
            case 'ERR.GP.INVALID_REQUEST':
            case 'ERR.GP.OVER_QUERY_LIMIT':
            case 'ERR.GP.REQUEST_DENIED':
            case 'ERR.GP.UNKNOWN_ERROR':
            case 'ERR.GP.ZERO_RESULTS':
                msg = {
                    title: '',
                    message: ''
                };
                break;

            case ApiErrorCodes.ErrNoResponseFromApi:
                /*msg = {
                    title: this.$translate.instant('GENERALERROR_TITLE'),
                    message: `${statusMessage.code} - ${statusMessage.descrip} 
                                  ${this.$translate.instant('ERROR_NORESPONSEFROMAPI_MESSAGE')}`
                };*/
                msg = {
                    title: this.$translate.instant('GENERALERROR_TITLE'),
                    message: `${this.$translate.instant('ERROR_NORESPONSEFROMAPI_MESSAGE')}`
                };
                break;

            case ErrorCodes.ErrTimeout:
                /*msg = {
                    title: this.$translate.instant('GENERALERROR_TITLE'),
                    message: `${statusMessage.code} - ${statusMessage.descrip} 
                                  ${this.$translate.instant('ERROR_TIMEOUT_MESSAGE')}`
                };*/
                msg = {
                    title: this.$translate.instant('GENERALERROR_TITLE'),
                    message: `${this.$translate.instant('ERROR_TIMEOUT_MESSAGE')}`
                };
                break;

            case ErrorCodes.ErrInternetDisconnected:
                /*msg = {
                    title: this.$translate.instant('GENERALERROR_TITLE'),
                    message: `${statusMessage.code} - ${statusMessage.descrip}
                                  ${this.$translate.instant('ERROR_INTERNET_CONNECTION')}`
                };*/
                msg = {
                    title: this.$translate.instant('GENERALERROR_TITLE'),
                    message: `${this.$translate.instant('ERROR_INTERNET_CONNECTION')}`
                };
                break;

            case ApiErrorCodes.ErrBadRequest:
            case ApiErrorCodes.ErrInternalServerError:
            case ApiErrorCodes.ErrAuthAccessTokenInvalid:
            case ApiErrorCodes.ErrAuthInvalidAppKey:
            case ApiErrorCodes.ErrAuthUnsupportedGrantType:
            case ApiErrorCodes.ErrAuthAuthorizationHeader:
            default:
                /* 
                msg = {
                    title: this.$translate.instant('GENERALERROR_TITLE'),
                    message: `${statusMessage.code} - ${statusMessage.descrip} 
                                  ${this.$translate.instant('GENERALERROR_MESSAGE', { errorUniqueId: statusMessage.errorUniqueId })}`
                };
                */
                msg = {
                    title: this.$translate.instant('GENERALERROR_TITLE'),
                    message: `${this.$translate.instant('GENERALERROR_MESSAGE', { errorUniqueId: statusMessage.errorUniqueId })}`
                };
                break;
        }

        return msg;
    }
    
    /**
     * Muestra un dialog de tipo alert con el mensaje especificado.
     * 
     * @param {IErrorMessage} message Mensaje a mostrar al usuario.
     * @param {string} [keyTextButton='CANCEL_BUTTON_TEXT'] Key I18N para el texto del boton de OK.
     * @returns {angular.IPromise<void>}  Promesa de cierre del dialog.
     * @memberof ErrorHandlerService
     */
    public showError(message: IErrorMessage, keyTextButton: string = 'CANCEL_BUTTON_TEXT'): angular.IPromise<void> {
        const deferred: angular.IDeferred<void> = this.$q.defer<void>();
        
        if (message && message.message !== '') {
            const dialog: angular.material.IAlertDialog = this.$mdDialog.alert();
            dialog
                .title(message.title)
                .textContent(message.message)
                .ok(this.$translate.instant(keyTextButton))
                .hasBackdrop(true)
                .clickOutsideToClose(false)
                .escapeToClose(false);
            this.$mdDialog.show(dialog).finally(() => {
                deferred.resolve();
            });
        } else {
            deferred.resolve();
        }

        return deferred.promise;
    }
}