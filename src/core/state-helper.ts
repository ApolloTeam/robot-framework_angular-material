// tslint:disable:no-any

'use strict';

import angular from 'angular';
import { IInjectable } from '@uirouter/angularjs';
import { I18nService } from './i18n.service';
import { LazySystemService } from './lazy-system.service';

/**
 * Helper para devolver la configuracion de carga de los states.
 * @export
 * @class StateHelper
 */
export class StateHelper {

    /**
     * Declaración de dependencias.
     * @static
     * @type {string[]}
     * @memberof StateHelper
     */
    public static $inject: string[] = [
        '$log',
        '$q',
        LazySystemService.name,
        I18nService.name
    ];

    /**
     * Creates an instance of StateHelper.
     * @param {angular.ILogService} $log Servicio de logs.
     * @param {angular.IQService} $q Servicio para administrar el manejo de promesas.
     * @param {LazySystemService} lazySystem Carga módulos o archivos y los registra en el contexto de angular.
     * @param {I18nService} i18nService Helper para $translate.
     * @memberof StateHelper
     */
    constructor(
        private $log: angular.ILogService,
        private $q: angular.IQService,
        private lazySystem: LazySystemService,
        private i18nService: I18nService) {

        this.$log.debug(`${LazySystemService.name}::ctor`);
    }

    /**
     * Devuelve la configuración para cargar un modulo antes de mostrarlo.
     * @static
     * @param {string} invoker Nombre del módulo que esta invocando.
     * @param {string} modulePath Path del módulo que se va a activar.
     * @param {string} moduleName Nombre del módulo que se va a activar.
     * @param {string} viewName Nombre de la vista que se va a activar.
     * @returns {Object} Configuración que se utiliza en la propiedad resolve del state de la aplicación.
     * @memberof StateHelper
     */
    public static getResolveObject(invoker: string, modulePath: string, moduleName: string, viewName?: string): { [key: string]: IInjectable; } {
        // Resuelve la carga del módulo.
        // FIX: 2017-07-31 - (FAS / DAE) - $$animateJs is not a function
        // Link: https://github.com/ocombe/ocLazyLoad/issues/321
        // tslint:disable-next-line:typedef
        const resolverFunc = (
            $log: angular.ILogService,
            $q: angular.IQService,
            $$animateJs: any,
            lazySystem: LazySystemService,
            i18nService: I18nService) => {

            $log.debug(`${invoker}::resolve`);
            return lazySystem.loadModule(modulePath, moduleName).then(() => {
                let retI18n: angular.IPromise<void>;
                if (angular.isDefined(viewName)) {
                    retI18n = i18nService.configView(viewName);
                } else {
                    retI18n = $q.when(void 0);
                }

                return ret;
            });
        };

        // tslint:disable-next-line:no-unnecessary-local-variable
        const ret: { [key: string]: IInjectable; } = {
            loader: [
                '$log',
                '$q',
                '$$animateJs',
                LazySystemService.name,
                I18nService.name,
                resolverFunc]
        };
        return ret;
    }
}