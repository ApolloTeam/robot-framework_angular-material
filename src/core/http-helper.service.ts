// tslint:disable:no-any

'use strict';

import angular from 'angular';
import { Injectable } from 'angular-ts-decorators';
import { IHttpResolverService } from './http-resolver.service';
import { IStatusMessage } from './entities/status-message.entity';
import { IPromiseCancelable } from './entities/promise-cancelable';
import { IHttpPromiseCancelable } from './entities/http-promise-cancelable';
import { ErrorCodes } from './entities/error-codes';
import { Environments } from './entities/environments';

/**
 *  Helper para http.
 * @class HttpHelperService
 * @implements {IHttpHelperService}
 */
@Injectable()
export class HttpHelperService {

    /**
     * Declaración de dependencias.
     * @static
     * @type {string[]}
     * @memberOf HttpHelperService
     */
    public static $inject: string[] = [
        '$log',
        '$q',
        '$http',
        '$timeout',
        'HttpResolverService'
    ];

    /**
     * Creates an instance of HttpHelperService.
     * @param {angular.ILogService} $log Servicio de logs.
     * @param {angular.IQService} $q Servicio para administrar el manejo de promesas.
     * @param {angular.IHttpService} $http Facilita la comunicacion con servidores HTTP remotos. 
     * @param {angular.ITimeoutService} $timeout Ejecuta una accion despues de un intervalo de tiempo.
     * @param {IHttpResolverService} httpResolverService Helper que resuelve las peticiones http segun los environments.
     * @memberOf HttpHelperService
     */
    constructor(
        private $log: angular.ILogService,
        private $q: angular.IQService,
        private $http: angular.IHttpService,
        private $timeout: angular.ITimeoutService,
        private httpResolverService: IHttpResolverService) {
        this.$log.debug(`${HttpHelperService.name}::ctor`);
    }

    /**
     * Crea una petición http que se puede cancelar. Cuando es GET, si se demora, se cancela automáticamente.
     * @template T 
     * @param {string} invoker Nombre del componente que lo invoca.
     * @param {angular.IRequestConfig} config Configuracion http.
     * @returns {IPromiseCancelable<T>} Promesa cancelable.
     * @memberof HttpHelperService
     */
    public makeCancelable<T>(invoker: string, config: angular.IRequestConfig): IPromiseCancelable<T> {
        this.$log.debug(`${HttpHelperService.name}::makeCancelable`);
        const cancelDeferred: angular.IDeferred<IStatusMessage> = this.$q.defer<IStatusMessage>();
        const deferred: angular.IDeferred<T | IStatusMessage> = this.$q.defer<T | IStatusMessage>();
        config.timeout = cancelDeferred.promise;
        const cancelable: IHttpPromiseCancelable<T> = <IHttpPromiseCancelable<T>>this.$http(config);
        cancelable.cancel = () => {
            const d: Date = new Date();
            const sm: IStatusMessage = {
                code: '000',
                descrip: 'UserCancel',
                errorCode: <any>ErrorCodes.ErrUserCancel,
                errorUniqueId: 'E98F733E958344B2A87707C5B5B3347A',
                message: 'User cancel',
                timeStamp: d.toISOString()
            };
            cancelDeferred.resolve(sm);
        };
        cancelable.then((resp) => {
            this.$log.debug(`${invoker} (then) %o`, resp.data);
            this.httpResolverService.resolvePromise(deferred, resp.data);
        }).catch((resp) => {
            this.$log.debug(`${invoker} (catch) %o`, resp);
            const responseReject: Object | IStatusMessage = this.resolveReject(resp);
            this.httpResolverService.rejectPromise(deferred, responseReject);
        });

        if (config.method.toUpperCase() === 'GET') {
            this.$timeout(
                () => {
                    const d: Date = new Date();
                    const sm: IStatusMessage = {
                        code: '001',
                        descrip: 'RequestTimeout',
                        errorCode: <any>ErrorCodes.ErrTimeout,
                        errorUniqueId: 'E98F733E958344B2A87707C5B5B3347B',
                        message: 'Request Timeout',
                        timeStamp: d.toISOString()
                    };
                    cancelDeferred.resolve(sm);
                },
                20 * 1000);
        }

        const retCancelable: IPromiseCancelable<T> = <IPromiseCancelable<T>>deferred.promise;
        retCancelable.cancel = cancelable.cancel;
        return retCancelable;
    }

    /**
     * Cuando se cancela la promesa, resuelve lo que tiene que hacer.
     * @private
     * @template T 
     * @param {angular.IHttpPromiseCallbackArg<T>} resp Respuesta obtenida de la peticion al api.
     * @returns {(Object | IStatusMessage)} Objeto error o StatusMessage.
     * @memberof HttpHelperService
     */
    private resolveReject<T>(resp: angular.IHttpPromiseCallbackArg<T>): Object | IStatusMessage {
        this.$log.debug(`${HttpHelperService.name}::resolveReject`);
        let ret: Object | IStatusMessage = null;
        if (angular.isDefined(resp.data) && resp.data !== null) {
            ret = resp.data;
        } else if (angular.isDefined((<any>resp.config.timeout).$$state.value) && (<any>resp.config.timeout).$$state.status === 1) {
            ret = (<any>resp.config.timeout).$$state.value;
        } else if (resp.status === -1) {
            const d: Date = new Date();
            ret = {
                code: '002',
                descrip: 'InternetDisconected',
                errorCode: <any>ErrorCodes.ErrInternetDisconnected,
                errorUniqueId: 'E98F733E958344B2A87707C5B5B3347C',
                message: 'Internet Disconected',
                timeStamp: d.toISOString()
            };
        }

        return ret;
    }
}