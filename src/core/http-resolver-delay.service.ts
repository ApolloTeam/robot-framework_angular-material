'use strict';

import angular from 'angular';
import { Injectable } from 'angular-ts-decorators';
import { ConfigService } from '../app/app-config.service';
import { IHttpResolverService } from './http-resolver.service';

/**
 * Helper que resuelve las peticiones http segun los environments.
 * @export
 * @class HttpResolverService
 */
@Injectable()
export class HttpResolverService implements IHttpResolverService {

    /**
     * Declaración de dependencias.
     * @static
     * @type {string[]}
     * @memberOf HttpResolverService
     */
    public static $inject: string[] = [
        '$log',
        '$timeout',
        ConfigService.name
    ];

    /**
     * Creates an instance of HttpResolverService.
     * @param {angular.ILogService} $log Servicio de logs.
     * @param {angular.ITimeoutService} $timeout Ejecuta una accion despues de un intervalo de tiempo.
     * @param {ConfigService} configService Settings generales de la app.
     * @param {angular.IHttpService} $http Facilita la comunicacion con servidores HTTP remotos. 
     * @memberOf HttpResolverService
     */
    constructor(
        private $log: angular.ILogService,
        private $timeout: angular.ITimeoutService,
        private configService: ConfigService) {
        this.$log.debug(`${HttpResolverService.name}::ctor`);
    }

    /**
     * Resuelve la promesa condicionando el enviroment configurado.
     * @template TReturn 
     * @param {angular.IDeferred<TReturn>} deferred Promesa cancelable.
     * @param {TReturn} data response http.
     * @memberof HttpResolverService 
     */
    public resolvePromise<TReturn>(deferred: angular.IDeferred<TReturn>, data: TReturn): void {
        this.$log.debug(`${HttpResolverService.name}::resolvePromise`);
        const delay: number = this.configService.getDelayMock();
        this.$timeout(
            () => {
                deferred.resolve(data);
            },
            delay);
    }

    /**
     * Resuelve la promesa condicionando el enviroment configurado.
     * @template TReturn 
     * @param {angular.IDeferred<TReturn>} deferred Promesa cancelable.
     * @param {TReturn} data response http.
     * @memberof HttpResolverService 
     */
    public rejectPromise<TReturn>(deferred: angular.IDeferred<TReturn>, data: TReturn): void {
        this.$log.debug(`${HttpResolverService.name}::rejectPromise`);
        const delay: number = this.configService.getDelayMock();
        this.$timeout(
            () => {
                deferred.reject(data);
            },
            delay);
    }
}