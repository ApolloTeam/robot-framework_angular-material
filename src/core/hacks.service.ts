'use strict';

import angular from 'angular';
import { Injectable } from 'angular-ts-decorators';


@Injectable()
export class HacksService {

    /**
     * * Declaración de dependencias.
     * @static
     * @type {string[]}
     * @memberof HacksService
     */
    public static $inject: string[] = [
        '$log',
        '$document'
    ];

    /**
     * Creates an instance of HacksService.
     * @param {angular.ILogService} $log Servicio de logs.
     * @param {angular.IDocumentService} $document Wrapper del window.document.
     * @memberof HacksService
     */
    constructor(
        private $log: angular.ILogService,
        private $document: angular.IDocumentService) {

        this.$log.debug(`${HacksService.name}::ctor`);
    }

    /**
     * Quita el foco del elemento activo.
     * @memberof HacksService
     */
    public looseFocus(): void {
        this.$log.debug(`${HacksService.name}::looseFocus`);
        try {
            // tslint:disable-next-line:no-any
            const activeElement: any = this.$document[0].activeElement;
            activeElement.blur();            
        } catch (ex) {            
            // No hace nada.
        }
    }
}