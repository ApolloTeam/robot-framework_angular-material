'use strict';

import angular from 'angular';
import 'oclazyload';
import { NgModule, ModuleDecorated } from 'angular-ts-decorators';
import { ModuleHelper } from './module.helper';
import { I18nService } from './i18n.service';
import { LazySystemService } from './lazy-system.service';
import { StateHelper } from './state-helper';
import { HacksService } from './hacks.service';
import { HttpHelperService } from './http-helper.service';
import { ErrorHandlerService } from './error-handler.service';
import { HttpInterceptorService } from './http-interceptor.service';
import { HttpResolverService } from './http-resolver.service';
import './core.i18n';

/**
 * Módulo core.
 * @export
 * @class CoreModule
 * @implements {ModuleDecorated}
 */
@NgModule({
    declarations: [],
    providers: [
        StateHelper,
        HttpHelperService,
        I18nService,
        LazySystemService,
        ErrorHandlerService,
        HacksService,
        HttpInterceptorService,
        HttpResolverService],
    imports: ['oc.lazyLoad', 'core.i18n']
})
export class CoreModule implements ModuleDecorated {

    /**
     * Configuración del módulo.
     * @memberOf CoreModule
     */
    public config(): void {
        'ngInject';

        const $logger: angular.ILogService = ModuleHelper.getLogger();
        $logger.debug(`${CoreModule.name}::config`);
    }

    /**
     * Corre el módulo.
     * @memberof CoreModule
     */
    public run(): void {
        'ngInject';

        const $log: angular.ILogService = ModuleHelper.getLogger();
        $log.debug(`${CoreModule.name}::run`);
    }
}