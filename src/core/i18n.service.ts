'use strict';
import angular from 'angular';
import 'angular-translate';
import { Injectable } from 'angular-ts-decorators';

/**
 * Helper para $translate.
 * @class I18nService
 * @implements {II18nService}
 */
@Injectable()
export class I18nService {

    /**
     * Inyeccion de dependencias.
     * @static
     * @memberOf I18nService
     */
    public static $inject = [
        '$log',
        '$translate',
        '$translatePartialLoader'
    ];

    /**
     * Creates an instance of I18nService.
     * @param {angular.ILogService} $log Servicio de logs.
     * @param {angular.translate.ITranslateService} $translate Realiza localizaciÃ³n de texto.
     * @param {angular.translate.ITranslatePartialLoaderService} $translatePartialLoader Configura la ruta para la localizaciÃ³n de texto.
     * @memberOf I18nService
     */
    constructor(
        private $log: angular.ILogService,
        private $translate: angular.translate.ITranslateService,
        private $translatePartialLoader: angular.translate.ITranslatePartialLoaderService) {

        this.$log.debug(`${I18nService.name}::ctor`);
    }

    /**
     * Agrega el nombre del módulo especificado al servicio $translate.
     * @param {string} viewName Nombre de la vista que se va activar.
     * @returns {angular.IPromise<void>} Promesa.
     */
    public configView(viewName: string): angular.IPromise<void> {
        this.$log.debug(`${I18nService.name}::configView viewName %o`, viewName);
        this.$translatePartialLoader.addPart(viewName);
        return this.$translate.refresh();
    }

    /**
     * Devuelve la configuración para cargar las traducciones de una vista antes de mostrarla.
     * @static
     * @param {string} invoker Nombre del módulo que esta invocando.
     * @param {string} viewName Nombre de la vista que se va aactivar. 
     * @returns {Object} Configuración que se utiliza en la propiedad resolve del state de la aplicación. 
     * @memberOf I18nService
     */
    public static getResolveObject(invoker: string, viewName: string): Object {
        // tslint:disable-next-line:no-unnecessary-local-variable
        const ret: Object = {
            i18n: ['$log', I18nService.name, ($log: angular.ILogService, service: I18nService) => {
                $log.debug(`${invoker}::resolve`);
                return service.configView(viewName);
            }]
        };

        return ret;
    }
}