// tslint:disable:no-any

'use strict';

import angular from 'angular';
import 'oclazyload';
import { Injectable } from 'angular-ts-decorators';


/**
 * Carga módulos o archivos y los registra en el contexto de angular.
 * @export
 * @class LazySystemService
 */
@Injectable()
export class LazySystemService {


    /**
     * Declaración de dependencias.
     * @static
     * @type {string[]}
     * @memberof LazySystemService
     */
    public static $inject: string[] = [
        '$log',
        '$q',
        '$ocLazyLoad'
    ];

    /**
     * Diccionario que registra los modulos y archivos que ya fueron cargados por el servicio.
     * @private
     * @type {{ [key: string]: boolean }}
     * @memberof LazySystemService
     */
    private loaded: { [key: string]: boolean };

    /**
     * Creates an instance of LazySystemService.
     * @param {angular.ILogService} $log Servicio de logs.
     * @param {angular.IQService} $q Servicio para administrar el manejo de promesas.
     * @param {oc.ILazyLoad} $ocLazyLoad Servicio de carga de modulos.
     * @memberOf LazySystemService
     */
    constructor(
        private $log: angular.ILogService,
        private $q: angular.IQService,
        private $ocLazyLoad: oc.ILazyLoad) {

        this.$log.debug(`${LazySystemService.name}::ctor`);
        this.loaded = {};
    }

    /**
     * Carga un modulo y lo registra en el contexto de angular.
     * @param {string} src path del modulo.
     * @param {string} [name=null] name del modulo.
     * @returns {Promise<void>} Promesa con el módulo cargado.
     * @memberof LazySystemService
     */
    public loadModule(src: string, name: string = null): angular.IPromise<void> {
        const methodName: string = `${LazySystemService.name}::loadModule`;
        this.$log.debug(`${methodName} src %o name %o`, src, name);
        let ret: angular.IPromise<void>;

        if (angular.isDefined(this.loaded[src]) && this.loaded[src]) {
            ret = this.$q.when<void>(void 0);
        } else {
            ret = <angular.IPromise<void>>System.import(src).then((loadedFile: any) => {
                this.$log.debug(`${methodName} (then) src %o name %o`, src, name);
                return <angular.IPromise<void>>this.$ocLazyLoad.load(loadedFile[name].module).then(() => {
                    this.loaded[src] = true;
                });
            });
        }

        return ret;
    }

    /**
     * Carga un archivo y lo registra en el contexto de angular.
     * @param {string} src path del modulo.
     * @param {string} [name=null] name del modulo.
     * @returns {Promise<void>} Promesa con el archivo cargado.
     * @memberof LazySystemService
     */
    public load(src: string, name: string = null): angular.IPromise<void> {

        const methodName: string = `${LazySystemService.name}::load`;
        this.$log.debug(`${methodName} src %o name %o`, src, name);
        let ret: angular.IPromise<void>;

        if (angular.isDefined(this.loaded[src]) && this.loaded[src]) {
            ret = this.$q.when<void>(void 0);
        } else {
            ret = <angular.IPromise<void>>System.import(src).then((loadedFile: any) => {
                this.$log.debug(`${methodName} (then) src %o name %o`, src, name);
                return this.$ocLazyLoad.load(loadedFile[name]).then(() => {
                    this.loaded[src] = true;
                });
            });
        }

        return ret;
    }
}