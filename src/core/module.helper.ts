'use strict';
import angular from 'angular';

/**
 * Helper para los módulos angularjs.
 * @export
 * @class ModuleHelper
 */
// tslint:disable-next-line:no-stateless-class
export class ModuleHelper {

  /**
   * Devuelve el servicio de log de angularjs.
   * @static
   * @returns {angular.ILogService} El servicio de log de angularjs.
   * @memberof ModuleHelper
   */
  public static getLogger(): angular.ILogService {
    const injector: angular.auto.IInjectorService = angular.injector(['ng']);
    // tslint:disable-next-line:no-unnecessary-local-variable
    const $log: angular.ILogService = injector.get('$log');
    return $log;
  }

  /**
   * Decora la función especificada con las dependencias especificadas.
   * @param {void} func Función a decorar.
   * @param {string[]} dependencies Dependencias.
   * @returns {void} La función decorada.
   * @memberof ModuleHelper
   */
  public static decorate(func: () => void, dependencies: string[]): () => void {
    const $log: angular.ILogService = ModuleHelper.getLogger();
    $log.debug(`${ModuleHelper.name}::decorate %o`, func.name);
    func.$inject = dependencies || [];
    return func;
  }
}