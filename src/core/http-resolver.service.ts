'use strict';

import angular from 'angular';
import { Injectable } from 'angular-ts-decorators';

/**
 * Helper que resuelve las peticiones http segun los environments.
 * @export
 * @interface IHttpResolverService
 */
export interface IHttpResolverService {

    /**
     * Resuelve la promesa condicionando el enviroment configurado.
     * @template TReturn 
     * @param {angular.IDeferred<TReturn>} deferred Promesa cancelable.
     * @param {*} data response http.
     * @memberof IHttpResolverService 
     */
    resolvePromise<TReturn>(deferred: angular.IDeferred<TReturn>, data: TReturn): void;

    /**
     * Resuelve la promesa condicionando el enviroment configurado.
     * @template TReturn 
     * @param {angular.IDeferred<TReturn>} deferred Promesa cancelable.
     * @param {*} data response http.
     * @memberof IHttpResolverService 
     */
    rejectPromise<TReturn>(deferred: angular.IDeferred<TReturn>, data: TReturn): void;
}

/**
 * Helper que resuelve las peticiones http segun los environments.
 * @export
 * @class HttpResolverService
 */
@Injectable()
export class HttpResolverService implements IHttpResolverService {

    /**
     * Declaración de dependencias.
     * @static
     * @type {string[]}
     * @memberOf HttpResolverService
     */
    public static $inject: string[] = [
        '$log'
    ];

    /**
     * Creates an instance of HttpResolverService.
     * @param {angular.ILogService} $log Servicio de logs.
     * @memberOf HttpResolverService
     */
    constructor(
        private $log: angular.ILogService) {
        this.$log.debug(`${HttpResolverService.name}::ctor`);
    }

    /**
     * Resuelve la promesa condicionando el enviroment configurado.
     * @template TReturn 
     * @param {angular.IDeferred<TReturn>} deferred Promesa cancelable.
     * @param {*} data response http.
     * @memberof HttpResolverService 
     */
    public resolvePromise<TReturn>(deferred: angular.IDeferred<TReturn>, data: TReturn): void {
        this.$log.debug(`${HttpResolverService.name}::resolvePromise`);
        deferred.resolve(data);
    }

    /**
     * Resuelve la promesa condicionando el enviroment configurado.
     * @template TReturn 
     * @param {angular.IDeferred<TReturn>} deferred Promesa cancelable.
     * @param {*} data response http.
     * @memberof HttpResolverService 
     */
    public rejectPromise<TReturn>(deferred: angular.IDeferred<TReturn>, data: TReturn): void {
        this.$log.debug(`${HttpResolverService.name}::rejectPromise`);
        deferred.reject(data);
    }
}