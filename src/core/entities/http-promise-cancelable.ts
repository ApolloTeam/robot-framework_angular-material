'use strict';

import angular from 'angular';

/**
 * Promesa cancelable http.
 * @export
 * @interface IHttpPromiseCancelable
 * @extends {angular.IHttpPromise<T>}
 * @template T El tipo de la promesa.
 */
export interface IHttpPromiseCancelable<T> extends angular.IHttpPromise<T> {
    cancel: () => void;
}