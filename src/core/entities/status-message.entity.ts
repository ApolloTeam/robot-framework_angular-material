'use strict';

import angular from 'angular';

/**
 * Contiene un mensaje de estado para responder a una petición que produjo un error.
 * @export
 * @interface IStatusMessage
 */
export interface IStatusMessage {

    /**
     * HTTP response status code (e.g. 401).
     * @type {string}
     */
    code: string;

    /**
     * HTTP response status code description (e.g. "Unauthorized").
     * @type {string}
     */
    descrip: string;

    /**
     * Date and time (UTC).
     * @type {string}
     */
    timeStamp: string;

    /**
     * Identificador único de error para poder referenciarlo con el log de excepciones.
     * @type {string}
     */
    errorUniqueId?: string;

    /**
     * Error code for business rules treatment (e.g. "ERR.AUTH.ACCESSTOKEN.INVALID").
     * @type {string}
     */
    errorCode?: string;
    
    /**
     * Error message (e.g. "AccessToken invalid.").
     * @type {string}
     */
    message: string;

    /**
     * List of validation errors.
     * @type {*}
     */
    /* tslint:disable-next-line: no-any Se deshabilita ya que no se establece el tipo de la variable. */
    validationErrors?: any;
}