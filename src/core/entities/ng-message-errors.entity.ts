/**
 * Diccionario para registrar los errores de validación en una vista.
 * @export
 * @enum {number}
 */
export interface INgMessageErrors {
    [key: string]: boolean;
}