// tslint:disable:no-any

/**
 * Constantes para definir los diferentes environments.
 * @export
 * @enum {number}
 */
export enum Environments {
    /**
     * Production.
     */
    Production = <any>'prod',

    /**
     * Release.
     */
    Testing = <any>'test',

    /**
     * Release.
     */
    Development = <any>'dev',

    /**
     * Mock.
     */
    Mock = <any>'mock'
}