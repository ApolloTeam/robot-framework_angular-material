/**
 * Mensaje a mostrar al usuario.
 * @export
 * @interface IErrorMessage
 */
export interface IErrorMessage {

    /**
     * Título para el error.
     * @type {string}
     * @memberof IErrorMessage
     */
    title: string;

    /**
     * Mensaje detallado del error.
     * @type {string}
     */
    message: string;
}