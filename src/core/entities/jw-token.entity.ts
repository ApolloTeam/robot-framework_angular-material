'use strict';

import angular from 'angular';

/**
 * Successful Authentication Response.
 * @export
 * @interface IJWToken
 */
export interface IJWToken {
    
    /**
     * Token type ("Bearer").
     * @type {string}
     */
    token_type: string;

    /**
     * Access Token (in JWT format).
     * @type {string}
     */
    access_token: string;

    /**
     * Expiration time of the Access Token in seconds since the response was generated.
     * @type {number}
     */
    expires_in: number;

    /**
     * ID Token.
     * @type {string}
     */
    id_token: string;
}