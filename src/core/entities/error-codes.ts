// tslint:disable:no-any

/**
 * Constantes para códigos de error de Excepciones y StatusMessage (core).
 * @export
 * @enum {number}
 */
export enum ErrorCodes {
    /** ?. */ 
    ErrUserCancel = <any>'ERR.USER.CANCEL',

    /** ?. */ 
    ErrTimeout = <any>'ERR.TIMEOUT',

    /** ?. */ 
    ErrInternetDisconnected = <any>'ERR.INTERNET.DISCONNECTED'
}