'use strict';

import angular from 'angular';

/**
 * Promesa cancelable.
 * @export
 * @interface IPromiseCancelable
 * @extends {angular.IPromise<T>}
 * @template T El tipo de la promesa.
 */
export interface IPromiseCancelable<T> extends angular.IPromise<T> {
    cancel: () => void;
}