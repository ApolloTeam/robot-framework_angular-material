'use strict';
import angular from 'angular';
import { Injectable } from 'angular-ts-decorators';
import { AuthManager } from '../app/auth/auth.manager';
import { IStatusMessage } from './entities/status-message.entity';


/**
 * Http Interceptor.
 * @class HttpInterceptorService
 * @implements {IHttpInterceptorService}
 */
@Injectable()
export class HttpInterceptorService implements angular.IHttpInterceptor {

    /**
     * Inyeccion de dependencias.
     * @static
     * @memberOf HttpInterceptorService
     */
    public static $inject = [
        '$log',
        '$q',
        '$injector'
    ];

    /**
     * Creates an instance of HttpInterceptorService.
     * @param {ng.ILogService} $log Servicio de logs.
     * @param {ng.IQService} $q Ejecuta funciones de forma asíncrona, y utiliza sus valores de retorno cuando se hacen procesamiento.
     * @param {ng.auto.IInjectorService} $injector Permite obtener servicios inyectados previamente.
     * @memberOf HttpInterceptorService
     */
    constructor(
        private $log: angular.ILogService,
        private $q: angular.IQService,
        private $injector: angular.auto.IInjectorService) {
        this.$log.debug(`${HttpInterceptorService.name}::ctor`);

        this.request = this.requestInternal.bind(this);
    }

    public request: (config: angular.IRequestConfig) => angular.IRequestConfig | angular.IPromise<angular.IRequestConfig>;

    public requestInternal(config: angular.IRequestConfig): angular.IPromise<angular.IRequestConfig> {
        this.$log.debug(`${HttpInterceptorService.name}::requestInternal %o %o`, config.method, config.url);
        const deferred: angular.IDeferred<angular.IRequestConfig> = this.$q.defer<angular.IRequestConfig>();

        if (!/\/connect\/token|\/auth\/.*\/passwordresetrequest|\.(html?|js|json)/i.test(config.url)) {
            const authManager: AuthManager = this.$injector.get<AuthManager>('AuthManager');
            authManager.getAccessToken().then((accessToken: string) => {
                // tslint:disable-next-line:no-string-literal Se deshabilita ya que se requiere string literal.
                config.headers['Authorization'] = `Bearer ${accessToken}`;
                deferred.resolve(config);
            });
        } else {
            deferred.resolve(config);
        }

        return deferred.promise;
    }
}