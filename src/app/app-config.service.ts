// tslint:disable:no-http-string
'use strict';

import angular from 'angular';
import { Injectable } from 'angular-ts-decorators';
import { Environments } from '../core/entities/environments';

/**
 * Settings generales de la app.
 * @class ConfigService
 * @implements {IConfigService}
 */
@Injectable()
export class ConfigService {

    /**
     * Declaración de dependencias.
     * @static
     * @memberOf ConfigService
     */
    public static $inject = ['$log'];

    /**
     * Versión de la app.
     * @private
     * @type {string}
     * @memberof ConfigService
     */
    private version: string = '1.0.0';


    /**
     * Environment.
     * @private
     * @type {string}
     * @memberof ConfigService
     */
    // tslint:disable-next-line:no-any
    private environment: string = <any>Environments.Mock;

    /**
     * Delay para respuestas mockeadas.
     * @private
     * @type {number}
     * @memberof ConfigService
     */
    private delayMock: number = 5000;

    /**
     * Protocolo por default.
     * @private
     * @type {string}
     * @memberOf ConfigService
     */
    private protocol: string = 'https://';

    /**
     * Dominio de la url de las API.
     * @private
     * @type {string}
     * @memberOf ConfigService
     */
    private domain: string = 'zwitcher.com';

    /**
     * Grant type.
     * @private
     * @type {string}
     * @memberof ConfigService
     */
    private grantType: string = 'password';

    /**
     * Creates an instance of ConfigService.
     * @param {ng.ILogService} $log Servicio de logs.
     * @memberOf ConfigService
     */
    constructor(private $log: angular.ILogService) {
        this.$log.debug(`${ConfigService.name}::ctor`);
    }

    /**
     * Devuelve la baseUrl del módulo auth de Zwitcher.
     * @param {string} resource resource.
     * @returns {string} La baseUrl.
     * @memberOf ConfigService
     */
    public getAuthUrl(): string {
        return `${this.protocol}auth.${this.domain}`;
    }

    /**
     * Devuelve la baseUrl del módulo booking de Zwitcher.
     * @param {string} resource resource.
     * @returns {string} La baseUrl.
     * @memberOf ConfigService
     */
    public getBookingUrl(): string {
        return `${this.protocol}booking.${this.domain}`;
    }

    /**
     * Devuelve la baseUrl del módulo location de Zwitcher.
     * @param {string} resource resource.
     * @returns {string} La baseUrl.
     * @memberOf ConfigService
     */
    public getLocationgUrl(): string {
        return `${this.protocol}location.${this.domain}`;
    }

    /**
     * Devuelve el appKey de la aplicación.
     * @returns {string} El appKey de la aplicación.
     * @memberOf ConfigService
     */
    public getAppKey(): string {
        return 'robot-framework_angular-material';
    }

    /**
     * Devuelve el grandType para login.
     * @returns {string} GrandType para login.
     * @memberof ConfigService
     */
    public getGrandType(): string {
        return this.grantType;
    }

    /**
     * Devuelve el grandType para login.
     * @returns {string} GrandType para login.
     * @memberof ConfigService
     */
    public getVersion(): string {
        return this.version;
    }

    /**
     * Devuelve el environment configurado.
     * @returns {string} Environment configurado.
     * @memberof ConfigService
     */
    public getEnvironment(): string {
        return this.environment;
    }

    public getDelayMock(): number {
        return this.delayMock;
    } 
}