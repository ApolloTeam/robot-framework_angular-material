'use strict';

import angular from 'angular';
import 'angular-material';
import 'angular-translate';
import { StateService } from '@uirouter/angularjs';
import { Component, Input } from 'angular-ts-decorators';
import { ErrorHandlerService } from '../../../core/error-handler.service';
import { HacksService } from '../../../core/hacks.service';
import { AuthManager } from '../auth.manager';
import { IPromiseCancelable } from '../../../core/entities/promise-cancelable';
import { IStatusMessage } from '../../../core/entities/status-message.entity';
import { ApiErrorCodes } from '../../../api/commons/api-error-codes';
import { ErrorCodes } from '../../../core/entities/error-codes';
import { IErrorMessage } from '../../../core/entities/error-message.entity';
import { LoginState } from '../login/login.states';
import { IPasswordResetRequestViewData } from './password-reset-request.viewData';
import { IPasswordResetRequestModel } from './password-reset-request.model';
import './password-reset-request.css';

/**
 * Componente para Password Reset Request.
 * @export
 * @class PasswordResetRequestComponent
 */
@Component({
    selector: 'passwordResetRequest',
    templateUrl: 'app/auth/password-reset-request/password-reset-request.view.html'
})
export class PasswordResetRequestComponent {

    /**
     * Declaración de dependencias.
     * @static
     * @type {string[]}
     */
    public static $inject: string[] = [
        '$log',
        '$translate',
        '$mdDialog',
        '$state',
        ErrorHandlerService.name,
        HacksService.name,
        AuthManager.name
    ];

    /**
     * Modelo de la vista password-reset-request.
     * @type {IPasswordResetRequestModel}
     * @memberof PasswordResetRequestComponent
     */
    public model: IPasswordResetRequestModel;

    /**
     * * ViewData para la vista password-reset-request.
     * @type {IPasswordResetRequestViewData}
     * @memberof PasswordResetRequestComponent
     */
    public viewData: IPasswordResetRequestViewData;

    /**
     * Interfaz para acceder a los controles del formulario.
     * @type {IPasswordResetRequestForm}
     * @memberof PasswordResetRequestComponent
     */
    public form: IPasswordResetRequestForm;

    /**
     * Almacena el request para luego poder cancelarlo.
     * @private
     * @type {(IPromiseCancelable<void | IStatusMessage>)}
     * @memberof PasswordResetRequestComponent
     */
    private requestHandler: IPromiseCancelable<void | IStatusMessage>;

    /**
     * Creates an instance of PasswordResetRequestComponent.
     * @param {angular.ILogService} $log Servicio de logs.
     * @param {angular.translate.ITranslateService} $translate Servicio para localización de textos.
     * @param {angular.material.IDialogService} $mdDialog Servicio de dialog.
     * @param {StateService} $state Estado que corresponde al lugar ubicado en la aplicacion.
     * @param {ErrorHandlerService} errorHandlerService Manejador de errores no controlados.
     * @param {HacksService} hacksService Servicio de hacks.
     * @param {AuthManager} authManager Administra las operaciones del módulo Auth.
     */
    constructor(
        private $log: angular.ILogService,
        private $translate: angular.translate.ITranslateService,
        private $mdDialog: angular.material.IDialogService,
        private $state: StateService,
        private errorHandlerService: ErrorHandlerService,
        private hacksService: HacksService,
        private authManager: AuthManager) {

        this.$log.debug(`${PasswordResetRequestComponent.name}::ctor`);
    }

    /**
     * Realiza request de reset password.
     * @memberof PasswordResetRequestComponent
     */
    public passwordResetRequest(): void {
        const methodName: string = `${PasswordResetRequestComponent.name}::passwordResetRequest`;
        this.$log.debug(methodName);

        this.viewData.inProgress = true;
        this.viewData.isLocked = true;

        // Hack que quita el foco del cualquier elemento.
        this.hacksService.looseFocus();

        // Password reset request
        this.requestHandler = this.authManager.passwordResetRequest(this.model);
        this.requestHandler
            .then(() => this.passwordResetRequestThen())
            .catch((error: IStatusMessage) => this.passwordResetRequestCatch(error))
            .finally(() => {
                this.$log.debug(`${methodName} (finally)`);

                this.viewData.inProgress = false;
            });
    }

    /**
     * Acciones a ejecutar cuando el servicio de passwordResetRequest devuelve success.
     * @private
     * @memberof PasswordResetRequestComponent
     */
    private passwordResetRequestThen(): void {
        const methodName: string = `${PasswordResetRequestComponent.name}::passwordResetRequestThen`;
        this.$log.debug(`${methodName} (then)`);

        this.showSuccessDialog();
    }

    /**
     * Acciones a ejecutar cuando el servicio de passwordResetRequest devuelve un error.
     * @private
     * @param {IStatusMessage} statusMessage Mensaje de error.
     * @memberof PasswordResetRequestComponent
     */
    private passwordResetRequestCatch(statusMessage: IStatusMessage): void {
        const methodName: string = `${PasswordResetRequestComponent.name}::passwordResetRequestCatch`;
        this.$log.debug(`${methodName} (catch) %o`, statusMessage);

        let msg: IErrorMessage = null;
        let textButton: string = 'ACCEPT_BUTTON_TEXT';

        // tslint:disable-next-line:no-any
        switch (<any>statusMessage.code) {
            case '404':
                msg = {
                    title: '',
                    message: this.$translate.instant('PASSWORD_RESET_REQUEST_ERR_NOT_FOUND')
                };
                break;
            default:
                msg = this.errorHandlerService.process(statusMessage);
                textButton = 'CANCEL_BUTTON_TEXT';
                break;
        }

        this.viewData.isLocked = false;

        // Muestra el mensaje si corresponde.
        this.errorHandlerService.showError(msg, textButton);
    }

    /**
     * Muestra mensaje de exito al restablecer la contraseña.
     */
    /**
     * Muestra mensaje de exito.
     * @private
     * @memberof PasswordResetRequestComponent
     */
    private showSuccessDialog(): void {
        const textDialog: string = this.$translate.instant('PASSWORD_RESET_REQUEST_SUCCESS');
        const dialog: angular.material.IAlertDialog = this.$mdDialog.alert();
        dialog
            .textContent(textDialog)
            .ok(this.$translate.instant('ACCEPT_BUTTON_TEXT'))
            .hasBackdrop(true)
            .clickOutsideToClose(false)
            .escapeToClose(false);

        this.$mdDialog.show(dialog).finally(() => {
            // tslint:disable-next-line:no-any
            this.$state.go(<any>LoginState.login);
        });
    }

    /**
     * Inicia el componente.
     * @memberof PasswordResetRequestComponent
     */
    public $onInit(): void {
        this.$log.debug(`${PasswordResetRequestComponent.name}::$onInit`);

        this.viewData = {
            inProgress: false,
            isLocked: false
        };

        this.model = {
            username: ''
        };
    }

    /**
     * Devuelve verdadero si los controles del formulario tienen datos válidos, 
     * el formulario no esté en progreso y no esté bloqueado.
     * Falso en caso contrario.
     * @private 
     * @returns {boolean} Verdadero si los controles del formulario tienen datos válidos, 
     * el formulario no esté en progreso y no esté bloqueado.
     * Falso en caso contrario.
     * @memberof PasswordResetRequestComponent
     */
    private isEnableForm(): boolean {
        return angular.isObject(this.form)
            && this.form.$valid
            && !this.viewData.inProgress
            && !this.viewData.isLocked;
    }
}

/**
 * Interfaz para acceder a los controles del formulario.
 * @interface IPasswordResetRequestForm
 * @extends {angular.IFormController}
 */
interface IPasswordResetRequestForm extends angular.IFormController {

    /**
     * Control username.
     * @type {angular.INgModelController}
     * @memberof IPasswordResetRequestForm
     */
    username: angular.INgModelController;
}