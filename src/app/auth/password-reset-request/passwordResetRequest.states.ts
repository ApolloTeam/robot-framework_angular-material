// tslint:disable:no-any

/**
 * Estados del módulo PasswordResetRequest.
 * @export
 * @enum {number}
 */
export enum PasswordResetRequestState {
    
    /**
     * Vista de password-reset-request.
     */
    passwordResetRequest= <any>'app.auth.passwordResetRequest'
}