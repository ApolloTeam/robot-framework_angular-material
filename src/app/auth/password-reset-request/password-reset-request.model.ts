'use strict';

/**
 * Modelo de la vista password-reset-request.
 * @export
 * @interface IPasswordResetRequestModel
 */
export interface IPasswordResetRequestModel {

    /**
     * Username.
     * @type {string}
     */
    username: string;
}