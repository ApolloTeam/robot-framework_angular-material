'use strict';

/**
 * ViewData para la vista password-reset-request.
 * @export
 * @interface IPasswordResetRequestViewData
 */
export interface IPasswordResetRequestViewData {
    inProgress: boolean;
    isLocked: boolean;
}