'use strict';

import angular from 'angular';
import 'angular-translate';
import { NgModule, ModuleDecorated } from 'angular-ts-decorators';
import { ModuleHelper } from '../../../core/module.helper';
import { CoreModule } from '../../../core/core.module';
import { PasswordResetRequestComponent } from './password-reset-request.component';

/**
 * Módulo para Password Reset Request.
 * @export
 * @class PasswordResetRequestModule
 * @implements {ModuleDecorated}
 */
@NgModule({
    declarations: [PasswordResetRequestComponent],
    providers: [],
    imports: [
        'pascalprecht.translate',
        CoreModule.name
    ]
})
export class PasswordResetRequestModule implements ModuleDecorated {

    /**
     * Configuración del módulo.
     * @memberof PasswordResetRequestModule
     */
    public config(): void {
        'ngInject';
        const $log: angular.ILogService = ModuleHelper.getLogger();
        $log.debug(`${PasswordResetRequestModule.name}::config`);
    }

    /**
     * Corre el módulo.
     * @memberof PasswordResetRequestModule
     */
    public run(): void {
        'ngInject';

        const $log: angular.ILogService = ModuleHelper.getLogger();
        $log.debug(`${PasswordResetRequestModule.name}::run`);
    }
}