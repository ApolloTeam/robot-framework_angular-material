// tslint:disable:no-any

'use strict';

import angular from 'angular';
import { StateProvider, Ng1StateDeclaration, IInjectable } from '@uirouter/angularjs';
import { NgModule, ModuleDecorated } from 'angular-ts-decorators';
import { ModuleHelper } from '../../../core/module.helper';
import { StateHelper } from '../../../core/state-helper';
import { PasswordResetRequestState } from './passwordResetRequest.states';

/**
 * Módulo password-reset-request-states.
 * @export
 * @class PasswordResetRequestStatesModule
 * @implements {ModuleDecorated}
 */
@NgModule({
    declarations: [],
    providers: [],
    imports: ['ui.router']
})
export class PasswordResetRequestStatesModule implements ModuleDecorated {

    /**
     * Configuración del módulo.
     * @param {StateProvider} $stateProvider Configuración de de los estados de la aplicación. 
     * @memberOf PasswordResetRequestStatesModule
     */
    public config($stateProvider: StateProvider): void {
        'ngInject';

        const $logger: angular.ILogService = ModuleHelper.getLogger();
        const methodName: string = `${PasswordResetRequestStatesModule.name}::config`;
        $logger.debug(methodName);

        const resolveState: { [key: string]: IInjectable; } = StateHelper.getResolveObject(PasswordResetRequestStatesModule.name, 'app/auth/password-reset-request/password-reset-request.module', 'PasswordResetRequestModule', 'app/auth/password-reset-request');

        const passwordResetRequestState: Ng1StateDeclaration = {
            name: <any>PasswordResetRequestState.passwordResetRequest,
            url: '/passwordresetrequest',
            views: {
                auth: {
                    template: '<password-reset-request flex layout="column"></password-reset-request>'
                }
            },
            resolve: resolveState
        };

        $stateProvider.state(passwordResetRequestState);
    }

    /**
     * Corre el módulo.
     * @memberof PasswordResetRequestStatesModule
     */
    public run(): void {
        'ngInject';

        const $log: angular.ILogService = ModuleHelper.getLogger();
        $log.debug(`${PasswordResetRequestStatesModule.name}::run`);
    }
}