// tslint:disable:no-any

/**
 * Estados del módulo Auth.
 * @export
 * @enum {number}
 */
export enum AuthState {
    
    /**
     * Vista abstracta de auth.
     */
    auth = <any>'app.auth'
}