'use strict';

import angular from 'angular';
import 'angular-localforage';
import 'angular-jwt';
import 'angularMoment';
import { NgModule, ModuleDecorated } from 'angular-ts-decorators';
import { ModuleHelper } from '../../core/module.helper';
import { AuthManager } from './auth.manager';
import { ApiModule } from '../../api/api.module';

/**
 * Módulo para Auth.
 * @preserve
 */
@NgModule({
    declarations: [],
    providers: [AuthManager],
    imports: [
        'LocalForageModule',
        'angular-jwt',
        'angularMoment',
        ApiModule]

})
export class AuthModule implements ModuleDecorated {

    /**
     * Configuración del módulo.
     */
    public config(): void {
        'ngInject';
        const $log: angular.ILogService = ModuleHelper.getLogger();
        $log.debug(`${AuthModule.name}::config`);
    }

    /**
     * Corre el módulo.
     */
    public run(): void {
        'ngInject';
        const $log: angular.ILogService = ModuleHelper.getLogger();
        $log.debug(`${AuthModule.name}::run`);
    }
}