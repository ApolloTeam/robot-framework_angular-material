// tslint:disable:no-any

'use strict';

import angular from 'angular';
import { StateProvider, Ng1StateDeclaration, Ng1ViewDeclaration, IInjectable} from '@uirouter/angularjs';
import { NgModule, ModuleDecorated } from 'angular-ts-decorators';
import { ModuleHelper } from '../../core/module.helper';
import { StateHelper } from '../../core/state-helper';
import { LoginStatesModule } from './login/login-states.module';
import { PasswordResetRequestStatesModule } from './password-reset-request/password-reset-request-states.module';
import { AuthState } from './auth.states';

/**
 * Módulo auth-states.
 * @export
 * @class AuthStatesModule
 * @implements {ModuleDecorated}
 */
@NgModule({
    declarations: [],
    providers: [],
    imports: ['ui.router',
        LoginStatesModule.name,
        PasswordResetRequestStatesModule.name]
})
export class AuthStatesModule implements ModuleDecorated {

    /**
     * Configuración del módulo.
     * @param {StateProvider} $stateProvider Configuración de de los estados de la aplicación. 
     * @memberOf AuthStatesModule
     */
    public config($stateProvider: StateProvider): void {
        'ngInject';

        const $logger: angular.ILogService = ModuleHelper.getLogger();
        const methodName: string = `${AuthStatesModule.name}::config`;
        $logger.debug(methodName);

        const resolveState: { [key: string]: IInjectable; } = StateHelper.getResolveObject(AuthStatesModule.name, 'app/auth/auth.module', 'AuthModule');

        // Asigna datos de route a la variable appState.
        const authState: Ng1StateDeclaration = {
            name: <any>AuthState.auth,
            url: '/auth',
            abstract: true,
            views: {
                app: {
                    template: '<div ui-view="auth" flex layout="column"></div>'
                }
            },
            resolve: resolveState
        };

        $stateProvider.state(authState);
    }

    /**
     * Corre el módulo.
     * @memberof AuthStatesModule
     */
    public run(): void {
        'ngInject';

        const $log: angular.ILogService = ModuleHelper.getLogger();
        $log.debug(`${AuthStatesModule.name}::run`);
    }
}