'use strict';

/**
 * Modelo para los datos del usuario
 * @export
 * @interface IAuthUserModel
 */
export interface IAuthUserModel {
    // Nombre del usuario.
    fullName: string;

    // Email del Usuario.
    email: string;

    // Compañia del Usuario.
    companyName: string;

    // Avatar del Usuario.
    avatar?: string;

    // Tiempo de expiration del token externo.
    exp: number;
}