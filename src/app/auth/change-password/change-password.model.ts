'use strict';

/**
 * Modelo de la vista change-password.
 * @export
 * @interface IChangePasswordModel
 */
export interface IChangePasswordModel {

    /**
     * Old password.
     * @type {string}
     */
    oldPassword: string;

    /**
     * New password.
     * @type {string}
     */
    newPassword: string;

    /**
     * Confirmation.
     * @type {string}
     */
    confirmation: string;
}