'use strict';

import angular from 'angular';
import 'angular-material';
import 'angular-translate';
import { StateService, Ng1StateDeclaration} from '@uirouter/angularjs';
import { Component, Input } from 'angular-ts-decorators';
import { ErrorHandlerService } from '../../../core/error-handler.service';
import { HacksService } from '../../../core/hacks.service';
import { AuthManager } from '../auth.manager';
import { IPromiseCancelable } from '../../../core/entities/promise-cancelable';
import { IStatusMessage } from '../../../core/entities/status-message.entity';
import { ApiErrorCodes } from '../../../api/commons/api-error-codes';
import { ErrorCodes } from '../../../core/entities/error-codes';
import { IErrorMessage } from '../../../core/entities/error-message.entity';
import { INgMessageErrors } from '../../../core/entities/ng-message-errors.entity';
import { LoginState } from '../login/login.states';
import { WelcomeState } from '../../welcome/welcome.states';
import { IChangePasswordViewData } from './change-password.viewData';
import { IChangePasswordModel } from './change-password.model';
import './change-password.css';

/**
 * Componente para Change Password.
 * @export
 * @class ChangePasswordComponent
 */
@Component({
    selector: 'changePassword',
    templateUrl: 'app/auth/change-password/change-password.view.html'
})
export class ChangePasswordComponent {

    /**
     * Declaración de dependencias.
     * @static
     * @type {string[]}
     */
    public static $inject: string[] = [
        '$log',
        '$translate',
        '$mdToast',
        '$state',
        ErrorHandlerService.name,
        HacksService.name,
        AuthManager.name
    ];

    /**
     * Modelo de la vista change-password.
     * @type {IChangePasswordModel}
     * @memberof ChangePasswordComponent
     */
    public model: IChangePasswordModel;

    /**
     * * ViewData para la vista change-password.
     * @type {IChangePasswordViewData}
     * @memberof ChangePasswordComponent
     */
    public viewData: IChangePasswordViewData;

    /**
     * Interfaz para acceder a los controles del formulario.
     * @type {IChangePasswordForm}
     * @memberof ChangePasswordComponent
     */
    public form: IChangePasswordForm;

    /**
     * Almacena el request para luego poder cancelarlo.
     * @private
     * @type {(IPromiseCancelable<void | IStatusMessage>)}
     * @memberof ChangePasswordComponent
     */
    private requestHandler: IPromiseCancelable<void | IStatusMessage>;

    /**
     * Diccionario para registrar los errores de validación en una vista.
     * @type {ErrorValidation}
     * @memberof ChangePasswordComponent
     */
    public errors: INgMessageErrors;

    /**
     * Creates an instance of ChangePasswordComponent.
     * @param {angular.ILogService} $log Servicio de logs.
     * @param {angular.translate.ITranslateService} $translate Servicio para localización de textos.
     * @param {angular.material.IToastService} $mdToast Servicio de toast.
     * @param {StateService} $state Estado que corresponde al lugar ubicado en la aplicacion.
     * @param {ErrorHandlerService} errorHandlerService Manejador de errores no controlados.
     * @param {HacksService} hacksService Servicio de hacks.
     * @param {AuthManager} authManager Administra las operaciones del módulo Auth.
     */
    constructor(
        private $log: angular.ILogService,
        private $translate: angular.translate.ITranslateService,
        private $mdToast: angular.material.IToastService,
        private $state: StateService,
        private errorHandlerService: ErrorHandlerService,
        private hacksService: HacksService,
        private authManager: AuthManager) {

        this.$log.debug(`${ChangePasswordComponent.name}::ctor`);
    }

    /**
     * Inicia el componente.
     * @memberof ChangePasswordComponent
     */
    public $onInit(): void {
        this.$log.debug(`${ChangePasswordComponent.name}::$onInit`);

        this.viewData = {
            inProgress: false,
            isLocked: false,
            reveal: {
                oldPassword: false,
                newPassword: false,
                confirmation: false
            }
        };

        this.model = {
            oldPassword: '',
            newPassword: '',
            confirmation: ''
        };

        this.errors = {
            minlength: false,
            'invalid-credentials': false,
            'not-matching-password': false
        };
    }

    /**
     * Evita pegar texto en los campos.
     * @param {MouseEvent} $event Mouse event.
     * @memberof ChangePasswordComponent
     */
    public noPaste($event: MouseEvent): void {
        $event.preventDefault();
    }

    /**
     * Revela u oculta el contenido del control oldPassword del form.
     * @param {MouseEvent} $event Mouse event.
     * @memberof ChangePasswordComponent
     */
    public toggleRevealOldPassword($event: MouseEvent): void {
        this.setRevealPass('oldPassword', this.getValueRevealPass($event));
    }

    /**
     * Revela u oculta el contenido del control NewPassword del form.
     * @param {MouseEvent} $event Mouse event.
     * @memberof ChangePasswordComponent
     */
    public toggleRevealNewPassword($event: MouseEvent): void {
        this.setRevealPass('newPassword', this.getValueRevealPass($event));
    }

    /**
     * Revela u oculta el contenido del control confirmation del form.
     * @param {MouseEvent} $event Mouse event.
     * @memberof ChangePasswordComponent
     */
    public toggleRevealConfirmation($event: MouseEvent): void {
        this.setRevealPass('confirmation', this.getValueRevealPass($event));
    }

    /**
     * Devuelve verdadero si el control oldPassword tiene algún error activo.
     * Falso en caso contrario.
     * @returns {boolean} Verdadero si el control oldPassword tiene algún error activo.
     * Falso en caso contrario.
     * @memberof ChangePasswordComponent 
     */
    public hasErrorInOldPassword(): boolean {
        return angular.isObject(this.form)
            && this.form.oldPassword.$touched
            && (this.form.oldPassword.$invalid || this.errors['invalid-credentials']);
    }

    /**
     * Devuelve verdadero si el control newPassword tiene algún error activo.
     * Falso en caso contrario.
     * @returns {boolean} Verdadero si el control newPassword tiene algún error activo.
     * Falso en caso contrario.
     * @memberof ChangePasswordComponent 
     */
    public hasErrorInNewPassword(): boolean {
        return angular.isObject(this.form)
            && this.form.newPassword.$touched
            && (this.form.newPassword.$invalid
                || this.errors.minlength);
    }

    /**
     * Devuelve verdadero si el control confirmation tiene algún error activo.
     * Falso en caso contrario.
     * @returns {boolean} Verdadero si el control confirmation tiene algún error activo.
     * Falso en caso contrario.
     * @memberof ChangePasswordComponent 
     */
    public hasErrorInConfirmation(): boolean {
        return angular.isObject(this.form)
            && this.form.confirmation.$touched
            && (this.form.confirmation.$invalid
                || this.errors['not-matching-password']);
    }

    /**
     * Devuelve verdadero si los controles del formulario tienen datos válidos, 
     * el formulario no esté en progreso y no esté bloqueado.
     * Falso en caso contrario.
     * @returns {boolean} Verdadero si los controles del formulario tienen datos válidos, 
     * el formulario no esté en progreso y no esté bloqueado.
     * Falso en caso contrario.
     * @memberof ChangePasswordComponent
     */
    public isEnableForm(): boolean {
        return angular.isObject(this.form)
            && this.form.$valid
            && !this.viewData.inProgress
            && !this.viewData.isLocked;
    }

    /**
     * Realiza request de change password.
     * @memberof ChangePasswordComponent
     */
    public changePassword(): void {
        const methodName: string = `${ChangePasswordComponent.name}::changePasswordRequest`;
        this.$log.debug(methodName);

        this.cleanErrors();

        if (this.isPasswordMatch()) {

            this.viewData.inProgress = true;
            this.viewData.isLocked = true;

            // Hack que quita el foco del cualquier elemento.
            this.hacksService.looseFocus();

            // Change password request.
            this.requestHandler = this.authManager.changePassword(this.model);
            this.requestHandler
                .then(() => this.changePasswordThen())
                .catch((error: IStatusMessage) => this.changePasswordCatch(error))
                .finally(() => {
                    this.$log.debug(`${methodName} (finally)`);

                    this.viewData.inProgress = false;
                });

        } else {
            this.errors['not-matching-password'] = true;
        }
    }

    /**
     * Acciones a ejecutar cuando el servicio de changePassword devuelve success.
     * @private
     * @memberof ChangePasswordComponent
     */
    private changePasswordThen(): void {
        const methodName: string = `${ChangePasswordComponent.name}::changePasswordThen`;
        this.$log.debug(`${methodName} (then)`);

        this.showSuccessToast();
    }

    /**
     * Acciones a ejecutar cuando el servicio de changePassword devuelve un error.
     * @private
     * @param {IStatusMessage} statusMessage Mensaje de error.
     * @memberof ChangePasswordComponent
     */
    private changePasswordCatch(statusMessage: IStatusMessage): void {
        const methodName: string = `${ChangePasswordComponent.name}::changePasswordCatch`;
        this.$log.debug(`${methodName} (catch) %o`, statusMessage);

        let msg: IErrorMessage = null;

        // tslint:disable-next-line:no-any
        switch (<any>statusMessage.errorCode) {

            case ApiErrorCodes.ErrAuthInvalidCredentials:
                msg = null;
                this.errors['invalid-credentials'] = true;
                this.form.oldPassword.$setTouched();
                break;

            case ApiErrorCodes.ErrAuthNewPasswordTooweak:
                msg = null;
                this.errors.minlength = true;
                this.form.newPassword.$setTouched();
                break;

            case ApiErrorCodes.ErrAuthNewPasswordNotmatchConfirmation:
                msg = null;
                this.errors['not-matching-password'] = true;
                this.form.newPassword.$setTouched();
                break;

            default:
                msg = this.errorHandlerService.process(statusMessage);
                break;
        }
        // Hbailita el formulario. Sse puede volver a enviar.
        this.viewData.isLocked = false;

        // Muestra el mensaje si corresponde.
        this.errorHandlerService.showError(msg);
    }

    /**
     * Muestra mensaje de exito.
     * @private
     * @memberof ChangePasswordComponent
     */
    private showSuccessToast(): void {
        const textToast: string = this.$translate.instant('CHANGE_PASSWORD_SUCCESS_MESSAGE');
        
        const toastConfig: angular.material.ISimpleToastPreset = this.$mdToast.simple()
            .toastClass('success')
            .textContent(textToast)
            .hideDelay(3000)
            .position('top right');

        this.$mdToast.show(toastConfig).finally(() => {
            // tslint:disable-next-line:no-any
            this.$state.go(<any>WelcomeState.welcome);
        });
    }

    /**
     * Revela u oculta el contenido de un input type password.
     * @param {string} key Clave del input.
     * @param {boolean} value valor booleano para mostrar o no el contenido.
     * @memberof ChangePasswordComponent
     */
    private setRevealPass(key: string, value: boolean): void {
        this.viewData.reveal[key] = value;
    }

    /**
     * Devuelve true si el evento es mousedown para mostrar el contenido del campo.
     * False de lo contrario.
     * @private
     * @param {MouseEvent} $event Mouse event.
     * @returns {boolean} True si el evento es mousedown para mostrar el contenido del campo.
     * False de lo contrario.
     * @memberof ChangePasswordComponent
     */
    private getValueRevealPass($event: MouseEvent): boolean {
        return $event.type === 'mousedown';
    }

    /**
     * Devuelve true si las contraseñas son iguales. False de lo contrario.
     * @private
     * @returns {boolean} True si las contraseñas son iguales. Falso de lo contrario.
     * @memberof ChangePasswordComponent
     */
    private isPasswordMatch(): boolean {
        return angular.isObject(this.model)
            && this.model.newPassword === this.model.confirmation;
    }

    private cleanErrors(): void {
        this.errors.minlength = false;
        this.errors['not-matching-password'] = false;
        this.errors['invalid-credentials'] = false;
    }
}

/**
 * Interfaz para acceder a los controles del formulario.
 * @interface IChangePasswordForm
 * @extends {angular.IFormController}
 */
interface IChangePasswordForm extends angular.IFormController {

    /**
     * Control oldPassword.
     * @type {angular.INgModelController}
     * @memberof ChangePasswordComponent
     */
    oldPassword: angular.INgModelController;

    /**
     * Control newPassword.
     * @type {angular.INgModelController}
     * @memberof ChangePasswordComponent
     */
    newPassword: angular.INgModelController;

    /**
     * Control confirmation.
     * @type {angular.INgModelController}
     * @memberof ChangePasswordComponent
     */
    confirmation: angular.INgModelController;
}