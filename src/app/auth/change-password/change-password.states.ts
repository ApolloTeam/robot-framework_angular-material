// tslint:disable:no-any

/**
 * Estados del módulo ChangePassword.
 * @export
 * @enum {number}
 */
export enum ChangePasswordState {

    /**
     * Vista de change-password.
     */
    changePassword = <any>'app.main.changePassword'
}