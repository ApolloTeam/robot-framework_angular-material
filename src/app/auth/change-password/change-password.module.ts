'use strict';

import angular from 'angular';
import 'angular-translate';
import { NgModule, ModuleDecorated } from 'angular-ts-decorators';
import { ModuleHelper } from '../../../core/module.helper';
import { CoreModule } from '../../../core/core.module';
import { AuthModule } from '../auth.module';
import { ChangePasswordComponent } from './change-password.component';

/**
 * Módulo para Change Password.
 * @export
 * @class ChangePasswordModule
 * @implements {ModuleDecorated}
 */
@NgModule({
    declarations: [ChangePasswordComponent],
    providers: [],
    imports: [
        'pascalprecht.translate',
        CoreModule.name,
        AuthModule.name
    ]
})
export class ChangePasswordModule implements ModuleDecorated {

    /**
     * Configuración del módulo.
     * @memberof ChangePasswordModule
     */
    public config(): void {
        'ngInject';
        const $log: angular.ILogService = ModuleHelper.getLogger();
        $log.debug(`${ChangePasswordModule.name}::config`);
    }

    /**
     * Corre el módulo.
     * @memberof ChangePasswordModule
     */
    public run(): void {
        'ngInject';

        const $log: angular.ILogService = ModuleHelper.getLogger();
        $log.debug(`${ChangePasswordModule.name}::run`);
    }
}