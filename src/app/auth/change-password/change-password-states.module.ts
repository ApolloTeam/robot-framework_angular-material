// tslint:disable:no-any

'use strict';

import angular from 'angular';
import { StateProvider, Ng1StateDeclaration, IInjectable } from '@uirouter/angularjs';
import { NgModule, ModuleDecorated } from 'angular-ts-decorators';
import { ModuleHelper } from '../../../core/module.helper';
import { StateHelper } from '../../../core/state-helper';
import { ChangePasswordState } from './change-password.states';

/**
 * Módulo change-password-states.
 * @export
 * @class ChangePasswordStatesModule
 * @implements {ModuleDecorated}
 */
@NgModule({
    declarations: [],
    providers: [],
    imports: ['ui.router']
})
export class ChangePasswordStatesModule implements ModuleDecorated {

    /**
     * Configuración del módulo.
     * @param {StateProvider} $stateProvider Configuración de de los estados de la aplicación. 
     * @memberOf ChangePasswordStatesModule
     */
    public config($stateProvider: StateProvider): void {
        'ngInject';

        const $logger: angular.ILogService = ModuleHelper.getLogger();
        const methodName: string = `${ChangePasswordStatesModule.name}::config`;
        $logger.debug(methodName);

        const resolveState: { [key: string]: IInjectable; } = StateHelper.getResolveObject(ChangePasswordStatesModule.name, 'app/auth/change-password/change-password.module', 'ChangePasswordModule', 'app/auth/change-password');

        const changePasswordState: Ng1StateDeclaration = {
            name: <any>ChangePasswordState.changePassword,
            url: '/changePassword',
            views: {
                content: {
                    template: '<change-password flex></change-password>'
                }
            },
            resolve: resolveState
        };

        $stateProvider.state(changePasswordState);
    }

    /**
     * Corre el módulo.
     * @memberof ChangePasswordStatesModule
     */
    public run(): void {
        'ngInject';

        const $log: angular.ILogService = ModuleHelper.getLogger();
        $log.debug(`${ChangePasswordStatesModule.name}::run`);
    }
}