'use strict';

/**
 * ViewData para la vista change-password.
 * @export
 * @interface IChangePasswordViewData
 */
export interface IChangePasswordViewData {
    inProgress: boolean;
    isLocked: boolean;
    reveal: {
        [key: string]: boolean;
    };
}