
// tslint:disable:no-any

/**
 * Storage keys
 * @export
 * @enum {number}
 */
export enum AuthStorageKeys {
    
    /**
     * IAuthUserModel del usuario logeado.
     */
    
    User = <any>'Auth::User',

    /**
     * Token de la sesión activa.
     */
    Token = <any>'Auth::AccessToken',

    
    /**
     * Fecha del inicio de la sesión.
     */
    LastLogin = <any>'Auth::LastLogin',

    
    /**
     * Nombre del usuario logeado.
     */
    Username = <any>'Auth::UserName'
}