// tslint:disable:no-any

/**
 * Estados del módulo Login.
 * @export
 * @enum {number}
 */
export enum LoginState {
    
    /**
     * Vista de login.
     */
    login = <any>'app.auth.login'
}