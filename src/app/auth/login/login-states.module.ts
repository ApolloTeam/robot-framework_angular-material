// tslint:disable:no-any

'use strict';

import angular from 'angular';
import { StateProvider, Ng1StateDeclaration, IInjectable } from '@uirouter/angularjs';
import { NgModule, ModuleDecorated } from 'angular-ts-decorators';
import { ModuleHelper } from '../../../core/module.helper';
import { StateHelper } from '../../../core/state-helper';
import { LoginState } from './login.states';

/**
 * Módulo login-states.
 * @export
 * @class LoginStatesModule
 * @implements {ModuleDecorated}
 */
@NgModule({
    declarations: [],
    providers: [],
    imports: ['ui.router']
})
export class LoginStatesModule implements ModuleDecorated {

    /**
     * Configuración del módulo.
     * @param {StateProvider} $stateProvider Configuración de de los estados de la aplicación. 
     * @memberOf LoginStatesModule
     */
    public config($stateProvider: StateProvider): void {
        'ngInject';

        const $logger: angular.ILogService = ModuleHelper.getLogger();
        const methodName: string = `${LoginStatesModule.name}::config`;
        $logger.debug(methodName);

        const resolveState: { [key: string]: IInjectable; } = StateHelper.getResolveObject(LoginStatesModule.name, 'app/auth/login/login.module', 'LoginModule', 'app/auth/login');

        const loginState: Ng1StateDeclaration = {
            name: <any>LoginState.login,
            url: '/login',
            views: {
                auth: {
                    template: '<login flex></login>'
                }
            },
            resolve: resolveState
        };
        $stateProvider.state(loginState);
    }

    /**
     * Corre el módulo.
     * @memberof LoginStatesModule
     */
    public run(): void {
        'ngInject';

        const $log: angular.ILogService = ModuleHelper.getLogger();
        $log.debug(`${LoginStatesModule.name}::run`);
    }
}