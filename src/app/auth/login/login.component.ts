'use strict';

import angular from 'angular';
import 'angular-material';
import 'angular-translate';
import { StateService } from '@uirouter/angularjs';
import { Component, Input } from 'angular-ts-decorators';
import { ErrorHandlerService } from '../../../core/error-handler.service';
import { HacksService } from '../../../core/hacks.service';
import { AuthManager } from '../auth.manager';
import { IPromiseCancelable } from '../../../core/entities/promise-cancelable';
import { IStatusMessage } from '../../../core/entities/status-message.entity';
import { ApiErrorCodes } from '../../../api/commons/api-error-codes';
import { ErrorCodes } from '../../../core/entities/error-codes';
import { IErrorMessage } from '../../../core/entities/error-message.entity';
import { IJsonWebToken } from '../../../api/auth/json-web-token';
import { WelcomeState } from '../../welcome/welcome.states';
import { ILoginModel } from './login.model';
import { ILoginViewData } from './login.viewData';
import './login.css';

/**
 * Componente para login.
 * @export
 * @class LoginComponent
 */
@Component({
    selector: 'login',
    templateUrl: 'app/auth/login/login.view.html'
})
export class LoginComponent {

    /**
     * Declaración de dependencias.
     * @static
     * @type {string[]}
     */
    public static $inject: string[] = [
        '$log',
        '$translate',
        '$state',
        ErrorHandlerService.name,
        HacksService.name,
        AuthManager.name
    ];

    /**
     * Modelo de la vista login.
     * @type {ILoginModel}
     * @memberof LoginComponent
     */
    public model: ILoginModel;

    /**
     * ViewData para la vista login.
     * @type {ILoginViewData}
     * @memberof LoginComponent
     */
    public viewData: ILoginViewData;

    /**
     * Interfaz para acceder a los controles del formulario.
     * @type {ILoginForm}
     * @memberof LoginComponent
     */
    public form: ILoginForm;

    /**
     * Almacena el request para luego poder cancelarlo.
     * @private
     * @type {(IPromiseCancelable<void | IStatusMessage>)}
     * @memberof LoginComponent
     */
    private requestHandler: IPromiseCancelable<void | IStatusMessage>;

    /**
     * Creates an instance of LoginComponent.
     * @param {angular.ILogService} $log Servicio de logs.
     * @param {angular.translate.ITranslateService} $translate Servicio para localización de textos.
     * @param {StateService} $state Estado que corresponde al lugar ubicado en la aplicacion.
     * @param {ErrorHandlerService} errorHandlerService Manejador de errores no controlados.
     * @param {HacksService} hacksService Servicio de hacks.
     * @param {AuthManager} authManager Administra las operaciones del módulo Auth.
     */
    constructor(
        private $log: angular.ILogService,
        private $translate: angular.translate.ITranslateService,
        private $state: StateService,
        private errorHandlerService: ErrorHandlerService,
        private hacksService: HacksService,
        private authManager: AuthManager) {

        this.$log.debug(`${LoginComponent.name}::ctor`);
        this.viewData = {
            inProgress: false,
            revealPassword: false
        };
    }

    /**
     * Loguea usuarios en la app.
     * @memberof LoginComponent
     */
    public login(): void {
        const methodName: string = `${LoginComponent.name}::login`;
        this.$log.debug(methodName);

        this.viewData.inProgress = true;

        // Hack que quita el foco del cualquier elemento.
        this.hacksService.looseFocus();

        // Login
        this.requestHandler = this.authManager.login(this.model);
        this.requestHandler
            .then(() => this.loginThen())
            .catch((error: IStatusMessage) => this.loginCatch(error))
            .finally(() => {
                this.$log.debug(`${methodName} (finally)`);

                this.viewData.inProgress = false;
            });
    }

    /**
     * Acciones a ejecutar cuando el servicio de login devuelve success.
     * @private
     * @memberof LoginComponent
     */
    private loginThen(): void {
        const methodName: string = `${LoginComponent.name}::loginThen`;
        this.$log.debug(`${methodName} (then)`);

        // tslint:disable-next-line:no-any
        this.$state.go(<any>WelcomeState.welcome);
    }

    /**
     * Acciones a ejecutar cuando el servicio de login devuelve un error.
     * @private
     * @param {IStatusMessage} statusMessage Mensaje de error.
     * @memberof LoginComponent
     */
    private loginCatch(statusMessage: IStatusMessage): void {
        const methodName: string = `${LoginComponent.name}::loginCatch`;
        this.$log.debug(`${methodName} (catch) %o`, statusMessage);

        let msg: IErrorMessage = null;
        let textButton: string = 'ACCEPT_BUTTON_TEXT';

        // tslint:disable-next-line:no-any
        switch (<any>statusMessage.errorCode) {
            case ApiErrorCodes.ErrAuthInvalidCredentials:
                msg = {
                    title: '',
                    message: this.$translate.instant('LOGIN_ERR_INVALID_CREDENTIALS')
                };
                break;

            case ApiErrorCodes.ErrAuthInanctiveAccount:
                msg = {
                    title: '',
                    message: this.$translate.instant('LOGIN_ERR_INACTIVE_ACCOUNT')
                };
                break;

            case ApiErrorCodes.ErrAuthNoLoginPrivilege:
                msg = {
                    title: '',
                    message: this.$translate.instant('LOGIN_ERR_NO_PRIVILEGE')
                };
                break;

            default:
                msg = this.errorHandlerService.process(statusMessage);
                textButton = 'CANCEL_BUTTON_TEXT';
                break;
        }

        // Muestra el mensaje si corresponde.
        this.errorHandlerService.showError(msg, textButton);
    }

    /**
     * Inicia el componente.
     * @memberof LoginComponent
     */
    public $onInit(): void {
        this.$log.debug(`${LoginComponent.name}::$onInit`);

        this.viewData = {
            inProgress: false,
            revealPassword: false
        };

        this.model = {
            username: '',
            password: '',
            rememberUser: false
        };
    }

    /**
     * Revela u oculta el contenido de un input type password.
     * @param {MouseEvent} $event Mouse event.
     * @memberof LoginComponent
     */
    public setRevealPass($event: MouseEvent): void {
        const value: boolean = this.getValueRevealPass($event);
        this.$log.debug(`${LoginComponent.name}::setRevealPass value %o`, value);
        this.viewData.revealPassword = value;
    }

    /**
     * Devuelve true si el evento es mousedown para mostrar el contenido del campo.
     * False de lo contrario.
     * @private
     * @param {MouseEvent} $event Mouse event.
     * @returns {boolean} True si el evento es mousedown para mostrar el contenido del campo.
     * False de lo contrario.
     * @memberof LoginComponent
     */
    private getValueRevealPass($event: MouseEvent): boolean {
        return $event.type === 'mousedown';
    }

    /**
     * Devuelve verdadero si los controles del formulario tienen datos válidos, 
     * el formulario no esté en progreso y no esté bloqueado.
     * Falso en caso contrario.
     * @private 
     * @returns {boolean} Verdadero si los controles del formulario tienen datos válidos, 
     * el formulario no esté en progreso y no esté bloqueado.
     * Falso en caso contrario.
     * @memberof LoginComponent
     */
    private isFormValid(): boolean {
        return angular.isObject(this.form)
            && this.form.$valid
            && !this.viewData.inProgress;
    }
}

/**
 * Interfaz para acceder a los controles del formulario.
 * @interface ILoginForm
 * @extends {angular.IFormController}
 */
interface ILoginForm extends angular.IFormController {

    /**
     * Control username.
     * @type {angular.INgModelController}
     * @memberof ILoginForm
     */
    username: angular.INgModelController;

    /**
     * Control password.
     * @type {angular.INgModelController}
     * @memberof ILoginForm
     */
    password: angular.INgModelController;

    /**
     * Control rememberUser
     * @type {angular.INgModelController}
     * @memberof ILoginForm
     */
    rememberUser: angular.INgModelController;
}