'use strict';

/**
 * Modelo de la vista login.
 * @export
 * @interface ILoginModel
 */
export interface ILoginModel {

    /**
     * Username.
     * @type {string}
     */
    username: string;
    
    /**
     * Password
     * @type {string}
     */
    password: string;

    /**
     * Remember user
     * @type {boolean}
     * @memberof ILoginModel
     */
    rememberUser: boolean;

    
}