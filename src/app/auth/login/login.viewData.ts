'use strict';

/**
 * ViewData para la vista login.
 * @export
 * @interface ILoginViewData
 */
export interface ILoginViewData {
    inProgress: boolean;
    revealPassword: boolean;
}