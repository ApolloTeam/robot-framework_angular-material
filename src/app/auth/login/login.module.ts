'use strict';

import angular from 'angular';
import 'angular-translate';
import { NgModule, ModuleDecorated } from 'angular-ts-decorators';
import { ModuleHelper } from '../../../core/module.helper';
import { CoreModule } from '../../../core/core.module';
import { LoginComponent } from './login.component';

/**
 * Módulo para login.
 * @export
 * @class LoginModule
 * @implements {ModuleDecorated}
 */
@NgModule({
    declarations: [LoginComponent],
    providers: [],
    imports: [
        'pascalprecht.translate',
        CoreModule.name
    ]
})
export class LoginModule implements ModuleDecorated {

    /**
     * Configuración del módulo.
     */
    public config(): void {
        'ngInject';
        const $log: angular.ILogService = ModuleHelper.getLogger();
        $log.debug(`${LoginModule.name}::config`);
    }

    /**
     * Corre el módulo.
     * @param {angular.translate.ITranslatePartialLoaderService} $translatePartialLoader Configura la ruta para la localización de texto.
     * @param {angular.translate.ITranslateService} $translate Servicio para localización de textos.
     * @memberof LoginModule
     */
     public run($translatePartialLoader: angular.translate.ITranslatePartialLoaderService,
                $translate: angular.translate.ITranslateService): void {

        const $log: angular.ILogService = ModuleHelper.getLogger();
        $log.debug(`${LoginModule.name}::run`);
    }
}