'use strict';

import angular from 'angular';
import 'angular-jwt';
import 'angular-localforage';
import 'moment';
import { Injectable } from 'angular-ts-decorators';
import { ApiService } from '../../api/api.service';
import { IPromiseCancelable } from '../../core/entities/promise-cancelable';
import { IStatusMessage } from '../../core/entities/status-message.entity';
import { IJsonWebToken } from '../../api/auth/json-web-token';
import { IMoment } from '../../@types/moment/index';
import { IAuthUserModel } from './auth-user.model';
import { ILoginModel } from './login/login.model';
import { ILoginRequest } from '../../api/auth/login-request.entity';
import { IChangePasswordModel } from './change-password/change-password.model';
import { IChangePasswordRequest } from '../../api/auth/change-password-request.entity';
import { IPasswordResetRequestModel } from './password-reset-request/password-reset-request.model';
import { AuthStorageKeys } from './auth-storage-keys';

// tslint:disable:no-any

/**
 * Administra las operaciones del módulo Auth.
 * @class AuthManager
 */
@Injectable()
export class AuthManager {

    /**
     * Declaración de dependencias.
     * @static
     * @memberOf AuthService
     */
    public static $inject = [
        '$log',
        '$q',
        '$localForage',
        'moment',
        'jwtHelper',
        '$state',
        ApiService.name
    ];

    /**
     * Usuario Loggeado
     * @private
     * @type {IAuthUserModel}
     * @memberOf AuthService
     */
    private user: IAuthUserModel = null;

    /**
     * Promesa con la informacion del usuario logeado.
     * @private
     * @type {angular.IPromise<any[]>}
     * @memberof AuthManager
     */
    private userDataPromise: angular.IPromise<any[]>;

    /** 
     * AccessToken que se obtiene desde el servicio.
     * @private
     * @type {Object}
     * @memberOf AuthService
     */
    private accessToken: string = null;

    /**
     * Keys Storages.
     * @private
     * @type {string[]}
     * @memberOf AuthManager
     */
    // tslint:disable-next-line:no-any
    private authStorageKeys: any[] = [AuthStorageKeys.User, AuthStorageKeys.Token, AuthStorageKeys.LastLogin, AuthStorageKeys.Username];

    /**
     * Creates an instance of AuthManager.
     * @param {angular.ILogService} $log Servicio de logs.
     * @param {ng.IQService} $q Ejecuta funciones de forma asíncrona, y utiliza sus valores de retorno cuando se hacen procesamiento.
     * @param {angular.localForage.ILocalForageService} $localForage Servicio para almacenar datos cross platfrom.
     * @param {IMoment} moment Facilita el formateo de fechas.
     * @param {ng.jwt.IJwtHelper} jwtHelper Decodifica los token.
     * @param {StateService} stateService Servicio para manejar los states.
     * @param {ApiService} apiService Brinda acceso a los modulos del API.
     */
    constructor(
        private $log: angular.ILogService,
        private $q: angular.IQService,
        private $localForage: angular.localForage.ILocalForageService,
        private moment: IMoment,
        private jwtHelper: ng.jwt.IJwtHelper,
        private stateService: any,
        private apiService: ApiService) {

        this.$log.debug(`${AuthManager.name}::ctor`);
        this.userDataPromise = this.$localForage.getItem(this.authStorageKeys);
    }

    /**
     * Realiza el proceso de autenticacion del usuario.
     * @param {ILoginModel} loginModel Modelo de la vista login.view.
     * @returns {(ng.IPromise<IJsonWebToken | IStatusMessage>)} Promesa de confirmacion o IStatusMessage.
     * @memberOf AuthManager
     */
    public login(loginModel: ILoginModel): IPromiseCancelable<void | IStatusMessage> {
        const methodName: string = `${AuthManager.name}::login`;
        this.$log.debug(`${methodName}`);

        const deferred: angular.IDeferred<void | IStatusMessage> = this.$q.defer<void | IStatusMessage>();
        const loginRequest: ILoginRequest = this.mapLoginModelToLoginRequest(loginModel);
        const loginPromise: IPromiseCancelable<IJsonWebToken | IStatusMessage> = this.apiService.auth().login(loginRequest);

        loginPromise.then((jwt: IJsonWebToken) => {
            this.$log.debug(`${methodName}::loginPromise (then)`);
            this.saveUserData(loginModel, jwt, deferred);
        });
        loginPromise.catch((statusMessage: IStatusMessage) => {
            this.$log.debug(`${methodName}::loginPromise (catch) %o`, statusMessage);
            this.user = null;
            this.$localForage.removeItem(this.authStorageKeys);
            deferred.reject(statusMessage);
        });

        const ret: IPromiseCancelable<void | IStatusMessage> = <IPromiseCancelable<void | IStatusMessage>>deferred.promise;
        ret.cancel = loginPromise.cancel;
        return ret;
    }

    /**
     * Change the password of the authenticated user.
     * @param {IChangePasswordModel} changePasswordModel Modelo de la vista change-password.view.
     * @returns {(IPromiseCancelable<void | IStatusMessage>)} Promesa de confirmacion o IStatusMessage.
     * @memberof AuthManager
     */
    public changePassword(changePasswordModel: IChangePasswordModel): IPromiseCancelable<void | IStatusMessage> {
        const methodName: string = `${AuthManager.name}::changePassword`;
        this.$log.debug(`${methodName}`);
        const changePasswordRequest: IChangePasswordRequest = this.mapChangePasswordModelToChangePasswordRequest(changePasswordModel);
        const ret: IPromiseCancelable<void | IStatusMessage> = this.apiService.auth().changePassword(changePasswordRequest);
        return ret;
    }

    /**
     * Verificara si el email es valido para el envio de link a restablecer contraseña.
     * @param {IPasswordResetRequestModel} passwordResetRequestModel Modelo de la vista password-reset-request.view.
     * @returns {(IPromiseCancelable<void | IStatusMessage>)} Promesa de confirmacion o IStatusMessage.
     * @memberof AuthManager
     */
    public passwordResetRequest(passwordResetRequestModel: IPasswordResetRequestModel): IPromiseCancelable<void | IStatusMessage> {
        const methodName: string = `${AuthManager.name}::passwordResetRequest`;
        this.$log.debug(`${methodName}`);

        const ret: IPromiseCancelable<void | IStatusMessage> = this.apiService.auth().passwordResetRequest(passwordResetRequestModel.username);
        return ret;
    }

    /**
     * Devuelve el objeto User.
     * @returns {IAuthUserModel} Objeto Usuario.
     * @memberOf AuthService
     */
    public getCurrentUser(): angular.IPromise<IAuthUserModel> {
        this.$log.debug(`${AuthManager.name}::getCurrentUser`);

        return this.userDataPromise.then((data: any[]) => {
            // Devuelvo la posicion 0 del array que contiene AuthStorageKeys.User.
            return data[0];
        });
    }

    /**
     * Devuelve el objeto AccessToken.
     * @returns {string} Objeto AccessToken.
     * @memberOf AuthManager
     */
    public getAccessToken(): angular.IPromise<string> {
        this.$log.debug(`${AuthManager.name}::getAccessToken`);

        return this.userDataPromise.then((data: any[]) => {
            // Devuelvo la posicion 1 del array que contiene AuthStorageKeys.Token.
            return data[1];
        });
    }

    /**
     * Devuelve una promesa si el usuario tiene permiso para acceder al state requerido.
     * Una promesa con el nombre del state al que es redirigido.
     * @param {string} nameState Nombre del state que se quiere ingresar.
     * @returns {(angular.IPromise<void | string>)} Una promesa con valor true si el usuario tiene permiso para acceder al state requerido.
     * Una promesa con el nombre del state al que es redirigido.
     * @memberof AuthManager
     */
    public isAuthorized(nameState: string): angular.IPromise<void | any> {
        const methodName: string = `${AuthManager.name}::isAuthorized`;
        this.$log.debug(`${methodName} %o`, nameState);
        const deferred: angular.IDeferred<void | any> = this.$q.defer<void>();
        if (nameState.indexOf('auth') > -1) {
            this.$log.debug(`${methodName} (hasPermission)`);
            deferred.resolve();
        } else {
            this.userDataPromise.then((data: any[]) => {
                const user: IAuthUserModel = data[0];
                if (angular.isObject(user)
                    && user !== null
                    && user.email !== '') {
                    deferred.resolve();
                } else {
                    const target: any = this.stateService.target('app.auth.login');
                    deferred.resolve(target);
                }
            }).catch(() => {
                const target: any = this.stateService.target('app.auth.login');
                deferred.resolve(target);
            });
        }

        return deferred.promise;
    }

    /**
     * Finaliza la sesion del usuario.
     * @returns {ng.IPromise<void>} Promesa de confirmacion.
     * @memberOf AuthManager
     */
    public logout(): angular.IPromise<void> {
        this.$log.debug(`${AuthManager.name}::logout`);
        const deferred: angular.IDeferred<void> = this.$q.defer<void>();
        this.$localForage.removeItem(this.authStorageKeys)
            .finally(() => {
                this.accessToken = '';
                this.user = null;
                this.userDataPromise = this.$q.defer<any[]>().promise;
                deferred.resolve();
            });

        return deferred.promise;
    }

    /**
     * Guarda la informacion del usuario logeado.
     * @private
     * @param {ILoginModel} loginModel Modelo de la vista login.
     * @param {IJsonWebToken} jwt Json web token.
     * @param {(angular.IDeferred<void | IStatusMessage>)} deferred Promesa de confirmacion o IStatusMessage.
     * @memberof AuthManager
     */
    private saveUserData(loginModel: ILoginModel, jwt: IJsonWebToken, deferred: angular.IDeferred<void | IStatusMessage>): void {
        const methodName: string = `${AuthManager.name}::saveUserData`;
        const dataAccessToken: JwtTokenCustom = <JwtTokenCustom>this.jwtHelper.decodeToken(jwt.access_token);
        this.user = this.mapJwtToAuthUserModel(dataAccessToken);
        this.accessToken = jwt.access_token;

        // Graba en el storage los datos del usuario.
        this.$localForage.setItem(
            this.authStorageKeys, [
                this.user,
                jwt.access_token,
                this.moment().millisecond(0).toISOString(),
                loginModel.username
            ]).then(() => {
                this.$log.debug(`${methodName}::localForage (then)`);
                this.userDataPromise = this.$localForage.getItem(this.authStorageKeys);
                deferred.resolve();
            }).catch((statusMessage: IStatusMessage) => {
                this.$log.debug(`${methodName}::localForage (catch) %o`, statusMessage);
                deferred.reject(statusMessage);
            }).finally(() => {
                this.userDataPromise = this.$localForage.getItem(this.authStorageKeys);
            });
    }

    /**
     * Mapea un loginModel a un loginRequest.
     * @private
     * @param {ILoginModel} loginModel Modelo de la vista login.view.
     * @returns {ILoginRequest} Login request.
     * @memberof AuthManager
     */
    private mapLoginModelToLoginRequest(loginModel: ILoginModel): ILoginRequest {
        this.$log.debug(`${AuthManager.name}::mapLoginModelToLoginRequest`);

        const loginRequest: ILoginRequest = {
            username: loginModel.username,
            password: loginModel.password,
            client_id: null,
            grant_type: null,
            scopes: null
        };

        return loginRequest;
    }

    /**
     * Mapea un changePasswordModel a un changePasswordRequest.
     * @private
     * @param {IChangePasswordModel} changePasswordModel Modelo de la vista change-password.view.
     * @returns {IChangePasswordRequest} change password request.
     * @memberof AuthManager
     */
    private mapChangePasswordModelToChangePasswordRequest(changePasswordModel: IChangePasswordModel): IChangePasswordRequest {
        this.$log.debug(`${AuthManager.name}::mapChangePasswordModelToChangePasswordRequest`);

        const changePasswordRequest: IChangePasswordRequest = {
            oldPassword: changePasswordModel.oldPassword,
            newPassword: changePasswordModel.newPassword,
            confirmation: changePasswordModel.confirmation
        };

        return changePasswordRequest;
    }

    /**
     * Devuelve un AuthUserModel armado con los datos del jwt.
     * @private
     * @param {*} jwt Accesstoken decodificado como objeto.
     * @returns {IAuthUserModel} Modelo del usuario autenticado.
     * @memberof AuthManager
     */
    private mapJwtToAuthUserModel(jwt: JwtTokenCustom): IAuthUserModel {
        this.$log.debug(`${AuthManager.name}::mapJwtToAuthUserModel`);

        const ret: IAuthUserModel = {
            fullName: <string>jwt.pax_fullname,
            email: <string>jwt.sub,
            companyName: <string>jwt.company_name,
            avatar: 'assets/img/user.png',
            exp: <number>jwt.exp
        };

        return ret;
    }

    public searchClients(searchText: string): angular.IPromise<string[]> {
        const ret: angular.IPromise<string[]> = this.apiService.auth().searchClients(searchText);
        return ret;
    }

}

/**
 * Jwt Token Custom.
 * @interface JwtTokenCustom
 * @extends {angular.jwt.JwtToken}
 */
interface JwtTokenCustom extends angular.jwt.JwtToken {
    pax_fullname: string;
    company_name: string;
}