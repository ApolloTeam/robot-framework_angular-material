'use strict';

import angular from 'angular';
import 'angular-material';
import 'angular-translate';
import { Component } from 'angular-ts-decorators';
import './app.css';

/**
 * Componente para la vista principal.
 * @export
 * @class AppComponent
 */
@Component({
    selector: 'app',
    templateUrl: 'app/app.view.html',
})
export class AppComponent {
    /**
     * Inyeccion de dependencias.
     * @static
     * @type {string[]}@memberof AppComponent
     */
    public static $inject: string[] = [
        '$log',
        '$translate'
    ];

    /**
     * Crea una instancia de AppComponent.
     * @param {angular.ILogService} $log Servicio de log.
     * @param {angular.translate.ITranslateService} $translate Servicio angular.translate.ITranslateService.
     * @memberof AppComponent
     */
    constructor(
        private $log: angular.ILogService,
        private $translate: angular.translate.ITranslateService) {

        this.$log.debug(`${AppComponent.name}::ctor`);
    }    

    /**
     * Captura el evento del click de los botones.
     * @param {MouseEvent} $event MouseEvent.
     * @memberof AppComponent
     */
    public onClickButton($event: MouseEvent): void {
        this.$log.debug(`${AppComponent.name}::onClickButton %o`, $event);
    }

    /**
     * Cambia el idioma de la aplicacion a español.
     * @param {MouseEvent} $event Evento del mouse.
     * @memberOf AppComponent
     */
    public changeToSpanish($event: MouseEvent): void {
        this.$log.debug(`${AppComponent.name}::changeToSpanish`);
        this.changeLanguage('es');
    }

    /**
     * Cambia el idioma de la aplicacion a ingles.
     * @param {MouseEvent} $event Evento del mouse.
     * @memberOf AppComponent
     */
    public changeToEnglish($event: MouseEvent): void {
        this.$log.debug(`${AppComponent.name}::changeToEnglish`);
        this.changeLanguage('en');
    }

    /**
     * Cambia el idioma de la aplicacion.
     * @param {string} language Idioma que se va a configurar.
     * @memberOf AppComponent
     */
    public changeLanguage(language: string): void {
        this.$log.debug(`${AppComponent.name}::changeLanguage language %o`, language);
        this.$translate.use(language).then(() => {
            this.$translate.refresh();
        });
    }

    /**
     * Devuelve verdadero si el lenguaje consultado es el activo., falso en caso contrario.
     * @param {string} language Lenguaje a verificar
     * @returns {boolean} Verdadero si el lenguaje esta activo, falso en caso contrario.
     * @memberOf AppComponent
     */
    public isActiveLanguage(language: string): boolean {
        return this.$translate.use() === language;
    }
}