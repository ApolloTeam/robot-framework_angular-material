// tslint:disable:no-any

'use strict';

import angular from 'angular';
import { TransitionService, UrlRouterProvider, StateProvider, Ng1StateDeclaration} from '@uirouter/angularjs';
import { NgModule, ModuleDecorated } from 'angular-ts-decorators';
import { ModuleHelper } from '../core/module.helper';
import { StateHelper } from '../core/state-helper';
import { I18nService } from '../core/i18n.service';
import { AuthManager } from './auth/auth.manager';
import { AuthModule } from './auth/auth.module';
import { AuthStatesModule } from './auth/auth-states.module';
import { MainStatesModule } from './main/main-states.module';
import { AppState } from './app.states';

/**
 * Módulo app-states.
 * @export
 * @class AppStatesModule
 * @implements {ModuleDecorated}
 */
@NgModule({
    declarations: [],
    providers: [],
    imports: ['ui.router',
        AuthStatesModule.name,
        MainStatesModule.name,
        AuthModule.name]
})
export class AppStatesModule implements ModuleDecorated {

    /**
     * Configuración del módulo.
     * @param {StateProvider} $stateProvider Configuración de de los estados de la aplicación.
     * @param {UrlRouterProvider} $urlRouterProvider Configuración de las url y estados de la aplicación.
     * @param {TransitionService} $transitionsProvider Servicio para manejar la transicion entre states.
     * @memberof AppStatesModule
     */
    public config($stateProvider: StateProvider,
                  $urlRouterProvider: UrlRouterProvider,
                  $transitionsProvider: TransitionService): void {
        'ngInject';

        const $logger: angular.ILogService = ModuleHelper.getLogger();
        const methodName: string = `${AppStatesModule.name}::config`;
        $logger.debug(methodName);

        // Obtiene la configuración del objeto resolve para cargar las traducciones antes de mostrar la vista.
        const resolveI18n: Object = I18nService.getResolveObject(AppStatesModule.name, 'app');

        // Asigna datos de route a la variable appState.
        const appState: Ng1StateDeclaration = <Ng1StateDeclaration>{
            name: <any>AppState.app,
            url: '',
            abstract: true,
            template: '<app flex layout="column"></app>',
            resolve: resolveI18n
        };

        // Configura las variables al State.
        $stateProvider.state(appState);

        // Configura la url predeterminada.
        $urlRouterProvider.otherwise('/auth/login');

        $transitionsProvider.onBefore({ to: '**' }, (trans) => {
            $logger.debug(`${methodName}::TransitionConfig %o`, trans.router.stateService);
            $logger.debug(`${methodName}::TransitionConfig %o`, trans.to());
            const authManager: AuthManager = trans.injector().get('AuthManager');
            const stateName: string = trans.to().name;
            return authManager.isAuthorized(stateName);
        });
    }

    /**
     * Corre el módulo.
     * @memberof AppStatesModule
     */
    public run(): void {
        'ngInject';

        const $log: angular.ILogService = ModuleHelper.getLogger();
        $log.debug(`${AppStatesModule.name}::run`);
    }
}