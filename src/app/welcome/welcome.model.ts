'use strict';

/**
 * Modelo de la vista welcome.
 * @export
 * @interface IWelcomeModel
 */
export interface IWelcomeModel {

    /**
     * Username.
     * @type {string}
     */
    username: string;
}