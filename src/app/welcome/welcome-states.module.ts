// tslint:disable:no-any

'use strict';

import angular from 'angular';
import { StateProvider, Ng1StateDeclaration, IInjectable } from '@uirouter/angularjs';
import { NgModule, ModuleDecorated } from 'angular-ts-decorators';
import { ModuleHelper } from '../../core/module.helper';
import { StateHelper } from '../../core/state-helper';
import { WelcomeState } from './welcome.states';

/**
 * Módulo Welcome States.
 * @export
 * @class WelcomeStatesModule
 * @implements {ModuleDecorated}
 */
@NgModule({
    declarations: [],
    providers: [],
    imports: ['ui.router']
})
export class WelcomeStatesModule implements ModuleDecorated {

    /**
     * Configuración del módulo.
     * @param {StateProvider} $stateProvider Configuración de de los estados de la aplicación.
     * @memberof WelcomeStatesModule
     */
    public config($stateProvider: StateProvider): void {
        'ngInject';

        const $logger: angular.ILogService = ModuleHelper.getLogger();
        const methodName: string = `${WelcomeStatesModule.name}::config`;
        $logger.debug(methodName);

        // Obtiene la configuración del objeto resolve para cargar las traducciones antes de mostrar la vista. 
        const resolveState: { [key: string]: IInjectable; } = StateHelper.getResolveObject(WelcomeStatesModule.name, 'app/welcome/welcome.module', 'WelcomeModule', 'app/welcome');

        const welcomeState: Ng1StateDeclaration = {
            name: <any>WelcomeState.welcome,
            url: '/',
            views: {
                content: {
                    template: '<welcome flex layout="column"></welcome>'
                }
            },
            resolve: resolveState
        };

        $stateProvider.state(welcomeState);

    }

    /**
     * Corre el módulo.
     * @memberof WelcomeStatesModule
     */
    public run(): void {
        'ngInject';

        const $log: angular.ILogService = ModuleHelper.getLogger();
        $log.debug(`${WelcomeStatesModule.name}::run`);
    }
}