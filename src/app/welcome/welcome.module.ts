'use strict';

import angular from 'angular';
import 'angular-localforage';
import 'angular-animate';
import 'angularMoment';
import 'angular-jwt';
import { NgModule, ModuleDecorated } from 'angular-ts-decorators';
import { ModuleHelper } from '../../core/module.helper';
import { ApiModule } from '../../api/api.module';
import { WelcomeComponent } from './welcome.component';

/**
 * Módulo para Welcome.
 * @preserve
 */
@NgModule({
    declarations: [WelcomeComponent],
    providers: [],
    imports: [
        'ngAnimate',
        ApiModule.name
    ]
})
export class WelcomeModule implements ModuleDecorated {

    /**
     * Configuración del módulo.
     */
    public config(): void {
        'ngInject';
        const $log: angular.ILogService = ModuleHelper.getLogger();
        $log.debug(`${WelcomeModule.name}::config`);
    }

    /**
     * Corre el módulo.
     */
    public run(): void {
        'ngInject';
        const $log: angular.ILogService = ModuleHelper.getLogger();
        $log.debug(`${WelcomeModule.name}::run`);
    }
}