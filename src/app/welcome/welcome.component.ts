'use strict';

import angular from 'angular';
import 'angular-material';
import 'angular-translate';
import { Component, Input } from 'angular-ts-decorators';
import { IWelcomeModel } from './welcome.model';
import './welcome.css';

/**
 * Componente para welcome.
 * @export
 * @class WelcomeComponent
 */
@Component({
    selector: 'welcome',
    templateUrl: 'app/welcome/welcome.view.html'
})
export class WelcomeComponent {

    /**
     * Declaración de dependencias.
     * @static
     * @type {string[]}
     */
    public static $inject: string[] = [
        '$log',
        '$translate'
    ];

    /**
     * Modelo de la vista welcome.
     * @type {IWelcomeModel}
     * @memberof WelcomeComponent
     */
    public model: IWelcomeModel;


    /**
     * Creates an instance of WelcomeComponent.
     * @param {angular.ILogService} $log Servicio de logs.
     * @param {angular.translate.ITranslateService} $translate Servicio para localización de textos.
     */
    constructor(
        private $log: angular.ILogService,
        private $translate: angular.translate.ITranslateService) {

        this.$log.debug(`${WelcomeComponent.name}::ctor`);
    }

    /**
     * Inicia el componente.
     * @memberof WelcomeComponent
     */
    public $onInit(): void {

        this.$log.debug(`${WelcomeComponent.name}::$onInit`);

        this.model = {
            username: 'Username'
        };
    }
}