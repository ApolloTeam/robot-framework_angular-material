// tslint:disable:no-any

/**
 * Estados del módulo Welcome.
 * @export
 * @enum {number}
 */
export enum WelcomeState {
    
    /**
     * Vista Welcome.
     */
    welcome = <any>'app.main.welcome'
}