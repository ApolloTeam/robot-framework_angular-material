// tslint:disable:no-any

'use strict';

import angular from 'angular';
import { StateProvider, Ng1StateDeclaration, IInjectable } from '@uirouter/angularjs';
import { NgModule, ModuleDecorated } from 'angular-ts-decorators';
import { ModuleHelper } from '../../core/module.helper';
import { StateHelper } from '../../core/state-helper';
import { AutomatizationState } from './automatization.states';

/**
 * Módulo Welcome States.
 * @export
 * @class AutomatizationStatesModule
 * @implements {ModuleDecorated}
 */
@NgModule({
    declarations: [],
    providers: [],
    imports: ['ui.router']
})
export class AutomatizationStatesModule implements ModuleDecorated {

    /**
     * Configuración del módulo.
     * @param {StateProvider} $stateProvider Configuración de de los estados de la aplicación.
     * @memberof AutomatizationStatesModule
     */
    public config($stateProvider: StateProvider): void {
        'ngInject';

        const $logger: angular.ILogService = ModuleHelper.getLogger();
        const methodName: string = `${AutomatizationStatesModule.name}::config`;
        $logger.debug(methodName);

        // Obtiene la configuración del objeto resolve para cargar las traducciones antes de mostrar la vista. 
        const resolveState: { [key: string]: IInjectable; } = StateHelper.getResolveObject(AutomatizationStatesModule.name, 'app/automatization/automatization.module', 'AutomatizationModule', 'app/automatization');

        const automatizatioState: Ng1StateDeclaration = {
            name: <any>AutomatizationState.automatization,
            url: '/automatization',
            views: {
                content: {
                    template: '<automatization flex layout="column"></automatization>'
                }
            },
            resolve: resolveState
        };

        $stateProvider.state(automatizatioState);

    }

    /**
     * Corre el módulo.
     * @memberof AutomatizationStatesModule
     */
    public run(): void {
        'ngInject';

        const $log: angular.ILogService = ModuleHelper.getLogger();
        $log.debug(`${AutomatizationStatesModule.name}::run`);
    }
}