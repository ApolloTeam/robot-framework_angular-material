'use strict';

import angular from 'angular';
import 'angular-material';
import 'angular-translate';
import { Component, Input } from 'angular-ts-decorators';
import { AuthManager } from '../auth/auth.manager';
import { IAutomatizationModel } from './automatization.model';
import './automatization.css';

/**
 * Componente para Automatization.
 * @export
 * @class AutomatizationComponent
 */
@Component({
    selector: 'automatization',
    templateUrl: 'app/automatization/automatization.view.html'
})
export class AutomatizationComponent {

    /**
     * Declaración de dependencias.
     * @static
     * @type {string[]}
     */
    public static $inject: string[] = [
        '$log',
        '$translate',
        '$mdToast',
        '$mdDialog',
        AuthManager.name
    ];

    /**
     * Modelo de la vista automatization.
     * @type {aAtomatizationModel}
     * @memberof AutomatizationComponent
     */
    public model: IAutomatizationModel;

    /**
     * Creates an instance of AutomatizationComponent.
     * @param {angular.ILogService} $log Servicio de logs.
     * @param {angular.translate.ITranslateService} $translate Servicio para localización de textos.
     * @param {angular.material.IToastService} $mdToast Servicio de Toast.
     * @param {angular.material.IDialogService} $mdDialog Servicio de Dialog.
     * @param {AuthManager} authManager Administra las operaciones del módulo Auth.
     * @memberof AutomatizationComponent
     */
    constructor(
        private $log: angular.ILogService,
        private $translate: angular.translate.ITranslateService,
        private $mdToast: angular.material.IToastService,
        private $mdDialog: angular.material.IDialogService,
        private authManager: AuthManager) {

        this.$log.debug(`${AutomatizationComponent.name}::ctor`);
    }

    /**
     * Inicia el componente.
     * @memberof AutomatizationComponent
     */
    public $onInit(): void {

        this.$log.debug(`${AutomatizationComponent.name}::$onInit`);

        this.model = {
            checkboxBoolean: true,
            checkboxValue: 'Todavia no hay nada',
            slider: 32,
            sliderMinMax: 90,
            sliderRange: 8,
            switchValue: 'Deshabilitado',
            selectNumber: null,
            countries: ['Brasil', 'Argentina', 'Peru'],
            selectedCountry: 'Argentina',
            cities: [
                { name: 'Buenos Aires', country: 'Argentina' },
                { name: 'La Pampa', country: 'Argentina' },
                { name: 'Catamarca', country: 'Argentina' },
                { name: 'Jujuy', country: 'Argentina' },
                { name: 'Chubut', country: 'Argentina' },
                { name: 'Rio de Janeiro', country: 'Brasil' },
                { name: 'Porto Alegre', country: 'Brasil' },
                { name: 'Sao Paulo', country: 'Brasil' },
                { name: 'Lima', country: 'Peru' },
                { name: 'Cuzco', country: 'Peru' }
            ],
            selectedCity: [],
            date: new Date(),
            postalCode: null,
            searchTextNameAutocomplete: null,
            searchTexCityAutocomplete: null,
            selectedCityAutocomplete: null,
            selectedNameAutocomplete: null,
            description: null,
            email: null
        };
    }

    public notWeekendsPredicate(date: Date): boolean {
        const day: number = date.getDay();
        return day !== 0 && day !== 6;
    }

    public showToast(className: string): void {
        this.$mdToast.show(
            this.$mdToast.simple()
                .toastClass(className)
                .textContent('Hola mundo!')
                .hideDelay(3000)
                .position('top right')
        );
    }

    public showDialog(): void {
        this.$mdDialog.show(
            this.$mdDialog.alert()
                //   .parent(angular.element(document.querySelector('#popupContainer')))
                .clickOutsideToClose(true)
                .title('Hola')
                .textContent('mundo.')
                .ok('Cerrar')
        );
    }

    public searchNames(searchText: string): angular.IPromise<string[]> {
        const ret: angular.IPromise<string[]> = this.authManager.searchClients(searchText);
        return ret;
    }

    public searchCities(searchText: string): string[] {
        searchText = searchText.toLowerCase();
        return this.model.cities.filter((city) => city.name.toLowerCase().indexOf(searchText)>-1 || city.country.toLowerCase().indexOf(searchText)>-1);
    }
}