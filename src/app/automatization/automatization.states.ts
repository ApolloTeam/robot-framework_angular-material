// tslint:disable:no-any

/**
 * Estados del módulo Automatization.
 * @export
 * @enum {number}
 */
export enum AutomatizationState {
    
    /**
     * Vista Automatization.
     */
    automatization = <any>'app.main.automatization'
}