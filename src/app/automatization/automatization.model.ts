'use strict';

// tslint:disable:no-any

/**
 * Modelo de la vista automatization.
 * @export
 * @interface IAutomatizationModel
 */
export interface IAutomatizationModel {
    checkboxBoolean: boolean;
    checkboxValue: string;
    slider: number;
    sliderMinMax: number;
    sliderRange: number;
    switchValue: string;
    selectNumber: number;
    countries: string[];
    selectedCountry: string;

    cities: any[];
    selectedCity: any[];
    selectedCityAutocomplete: any;
    selectedNameAutocomplete: string;
    date: Date;
    postalCode: number;
    searchTextNameAutocomplete: string;
    searchTexCityAutocomplete: string;
    description: string;
    email: string;
}