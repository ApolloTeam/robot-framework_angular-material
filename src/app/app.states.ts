// tslint:disable:no-any

/**
 * Estados del módulo App.
 * @export
 * @enum {number}
 */
export enum AppState {
    
    /**
     * Vista App.
     */
    app = <any>'app'
}