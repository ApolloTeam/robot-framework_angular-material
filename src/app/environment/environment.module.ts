'use strict';
import { NgModule, ModuleDecorated } from 'angular-ts-decorators';
import { ModuleHelper } from '../../core/module.helper';

/**
 * Módulo que importa configuraciones de ambiente.
 * @preserve
 * @export
 * @class EnvironmentModule
 * @implements {ModuleDecorated}
 */
@NgModule({
    declarations: [],
    providers: [],
    imports: []
})
export class EnvironmentModule implements ModuleDecorated {
    
    /**
     * Configuración del módulo.
     * @memberof EnvironmentModule
     */
    public config(): void {
        const $logger: angular.ILogService = ModuleHelper.getLogger();
        $logger.debug(`${EnvironmentModule.name}::config`);
    }

    /**
     * Corre el módulo.
     * @memberof EnvironmentModule
     */
    public run(): void {
        const $log: angular.ILogService = ModuleHelper.getLogger();
        $log.debug(`${EnvironmentModule.name}::run`);
    }
}