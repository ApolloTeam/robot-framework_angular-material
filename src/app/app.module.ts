'use strict';

import angular from 'angular';
import '@uirouter/angularjs';
import 'angular-translate';
import 'angular-animate';
import 'angular-translate-loader-partial';
import 'angular-material';
import 'angular-material-icons';
import 'angular1-zones';
import { NgModule, ModuleDecorated } from 'angular-ts-decorators';
import { ModuleHelper } from '../core/module.helper';
import { ConfigService } from './app-config.service';
import { I18nService } from '../core/i18n.service';
import { HttpInterceptorService } from '../core/http-interceptor.service';
import { CoreModule } from '../core/core.module';
import { EnvironmentModule } from './environment/environment.module';
import { AppStatesModule } from './app-states-module';
import { AppComponent } from './app.component';
import './app.templates';
import './app.i18n';

/**
 * Módulo principal de la app.
 * @preserve
 * @export
 * @class AppModule
 * @implements {ModuleDecorated}
 */
@NgModule({
    declarations: [AppComponent],
    providers: [ConfigService],
    imports: [
        'ui.router',
        'ngMessages',
        'pascalprecht.translate',
        'ngAnimate',
        'ngMaterial',
        'ngMdIcons',
        CoreModule.name,
        EnvironmentModule.name,
        AppStatesModule.name,
        'app.i18n',
        'app.templates'
    ]
})
export class AppModule implements ModuleDecorated {

    /**
     * Configuración del módulo.
     * @param {angular.translate.ITranslateProvider} $translateProvider Configuración de angular-translate.
     * @param {angular.material.IThemingProvider} $mdThemingProvider  Servicio Theming de angular-material.
     * @param {angular.IHttpProvider} $httpProvider Servicio para manejar request http.
     * @memberof AppModule
     */
    public config(
        $translateProvider: angular.translate.ITranslateProvider,
        $mdThemingProvider: angular.material.IThemingProvider,
        $httpProvider: angular.IHttpProvider): void {
        'ngInject';

        const $logger: angular.ILogService = ModuleHelper.getLogger();
        $logger.debug(`${AppModule.name}::config`);


        // Crea las paletas de colores negro y blanco.
        $mdThemingProvider.definePalette('black', {
            '50': '000000',
            '100': '000000',
            '200': '000000',
            '300': '000000',
            '400': '000000',
            '500': '000000',
            '600': '000000',
            '700': '000000',
            '800': '000000',
            '900': '000000',
            'A100': '000000',
            'A200': '000000',
            'A400': '000000',
            'A700': '000000',
            'contrastDefaultColor': 'light'
        });
        $mdThemingProvider.definePalette('white', {
            '50': 'ffffff',
            '100': 'ffffff',
            '200': 'ffffff',
            '300': 'ffffff',
            '400': 'ffffff',
            '500': 'ffffff',
            '600': 'ffffff',
            '700': 'ffffff',
            '800': 'ffffff',
            '900': 'ffffff',
            'A100': 'ffffff',
            'A200': 'ffffff',
            'A400': 'ffffff',
            'A700': 'ffffff',
            'contrastDefaultColor': 'dark'
        });
        $mdThemingProvider.theme('default')
            .primaryPalette('grey', {
                default: '600',
                'hue-1': '800',
                'hue-2': '900',
                'hue-3': '300'
            })
            .accentPalette('red', {
                default: '900'
            });

        // Define la url para localizar los archivos de i18n.
        // Cache de los JSON.
        // Link: https://fadeit.dk/blog/2015/04/09/preload-angular-translate-partial-loader/
        $translateProvider.useLoader('$translatePartialLoader', {
            urlTemplate: (part: string, lang: string): string => {
                // Prepara la url de la localizacion del módulo activo.
                // Soporta 2 formatos:
                // El el primero se puede indicar el nombre del archivo a cargar.
                // En el segundo se toma la ultima parte del nombre/ruta  de la
                // Vista/componente y se utiliza como nombre de archivo.
                // 1) Configuración: modulo/submodulo1/submodulo2@archivo
                // Archivo: modulo/submodulo1/submodulo2/i18n/archivo.i18n.es.json
                // 2) Configuración: modulo/submodulo1/submodulo2
                // Archivo: modulo/submodulo1/submodulo2/i18n/submodulo2.i18n.es.json
                let moduleName: string;
                let fileName: string;
                let patrsAux: string[];
                if (part.indexOf('@') > -1) {
                    patrsAux = part.split('@');
                    moduleName = patrsAux[0];
                    fileName = patrsAux[patrsAux.length - 1];
                } else {
                    patrsAux = part.split('/');
                    moduleName = part;
                    fileName = patrsAux[patrsAux.length - 1];
                }

                // tslint:disable-next-line:no-unnecessary-local-variable
                const ret: string = `${moduleName}/i18n/${fileName}.i18n.${lang}.json`;
                return ret;
            },
            $http: { cache: true } // Enable caching for PartialLoader
        });
        $translateProvider.useLoaderCache(true);

        // Asigna el idioma verificado de la aplicacion.
        $translateProvider.preferredLanguage('es');
        $translateProvider.fallbackLanguage('es');

        $httpProvider.interceptors.push(HttpInterceptorService.name);

    }

    /**
     * Corre el módulo.
     * @memberof AppModules
     * @param {angular.translate.ITranslatePartialLoaderService} $translatePartialLoader Configura la ruta para la localización de texto.
     * @param {angular.translate.ITranslateService} $translate Servicio para localización de textos.
     * @param {angular.IRootScopeService} $rootScope Contexto de ejecucion.
     */
    public run($translatePartialLoader: angular.translate.ITranslatePartialLoaderService,
               $translate: angular.translate.ITranslateService,
               $rootScope: angular.IRootScopeService): void {
        'ngInject';

        const $log: angular.ILogService = ModuleHelper.getLogger();
        $log.debug(`${AppModule.name}::run`);
    }
}