// tslint:disable:no-any

/**
 * Estados del módulo Main.
 * @export
 * @enum {number}
 */
export enum MainState {
    
    /**
     * Vista Main.
     */
    main = <any>'app.main'
}