// tslint:disable:no-any

'use strict';

import angular from 'angular';
import { StateProvider, Ng1StateDeclaration} from '@uirouter/angularjs';
import { NgModule, ModuleDecorated } from 'angular-ts-decorators';
import { ModuleHelper } from '../../core/module.helper';
import { StateHelper } from '../../core/state-helper';
import { WelcomeStatesModule } from '../welcome/welcome-states.module';
import { ChangePasswordStatesModule } from '../auth/change-password/change-password-states.module';
import { AutomatizationStatesModule } from '../automatization/automatization-states.module';
import { MainState } from './main.states';

/**
 * Módulo main-states.
 * @export
 * @class MainStatesModule
 * @implements {ModuleDecorated}
 */
@NgModule({
    declarations: [],
    providers: [],
    imports: ['ui.router',
        WelcomeStatesModule.name,
        ChangePasswordStatesModule.name,
        AutomatizationStatesModule.name
    ]
})
export class MainStatesModule implements ModuleDecorated {

    /**
     * Configuración del módulo.
     * @param {StateProvider} $stateProvider Configuración de de los estados de la aplicación.
     * @memberof MainStatesModule
     */
    public config($stateProvider: StateProvider): void {
        'ngInject';

        const $logger: angular.ILogService = ModuleHelper.getLogger();
        const methodName: string = `${MainStatesModule.name}::config`;
        $logger.debug(methodName);

        // Obtiene la configuración del objeto resolve para cargar las traducciones antes de mostrar la vista. 
        const resolveState: Object = StateHelper.getResolveObject(MainStatesModule.name, 'app/main/main.module', 'MainModule', 'app/main');

        const mainState: Ng1StateDeclaration = <any>{
            name: <any>MainState.main,
            abstract: true,
            url: '',
            views: {
                app: {
                    template: '<main flex layout="column"></main>'
                }
            },
            resolve: resolveState
        };

        $stateProvider.state(mainState);
    }

    /**
     * Corre el módulo.
     * @memberof MainStatesModule
     */
    public run(): void {
        'ngInject';

        const $log: angular.ILogService = ModuleHelper.getLogger();
        $log.debug(`${MainStatesModule.name}::run`);
    }
}