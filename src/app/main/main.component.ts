'use strict';

import angular from 'angular';
import 'angular-material';
import 'angular-translate';
import { ConfigService } from '../../app/app-config.service';
import { AuthManager } from '../auth/auth.manager';
import { Component, Input } from 'angular-ts-decorators';
import { IMainModel } from './main.model';
import { IMainViewData } from './main.viewData';
import { MainState } from './main.states';
import { LoginState } from '../auth/login/login.states';
import { WelcomeState } from '../welcome/welcome.states';
import { ChangePasswordState } from '../auth/change-password/change-password.states';
import { AutomatizationState } from '../automatization/automatization.states';
import { IAuthUserModel } from '../auth/auth-user.model';
import './main.css';

/**
 * Componente para main.
 * @export
 * @class MainComponent
 */
@Component({
    selector: 'main',
    templateUrl: 'app/main/main.view.html'
})
export class MainComponent {

    /**
     * Declaración de dependencias.
     * @static
     * @type {string[]}
     */
    public static $inject: string[] = [
        '$log',
        '$translate',
        '$mdSidenav',
        '$mdMedia',
        ConfigService.name,
        AuthManager.name
    ];

    /**
     * Modelo de la vista main.
     * @type {IMainModel}
     * @memberof MainComponent
     */
    public model: IMainModel;

    /**
     * ViewData para la vista main.
     * @type {IMainViewData}
     * @memberof MainComponent
     */
    public viewData: IMainViewData;

    /**
     * Creates an instance of MainComponent.
     * @param {angular.ILogService} $log Servicio de logs.
     * @param {angular.translate.ITranslateService} $translate Servicio para localización de textos.
     * @param {angular.material.ISidenavService} $mdSidenav Servicio angular.material.ISidenavService.
     * @param {angular.material.IMedia} $mdMedia Servicio angular.material.IMedia.
     * @param {IConfigService} configService Permite acceder a las urls que conectan con el api.
     * @param {AuthManager} authManager Administra las operaciones del módulo Auth.
     */
    constructor(
        private $log: angular.ILogService,
        private $translate: angular.translate.ITranslateService,
        private $mdSidenav: angular.material.ISidenavService,
        private $mdMedia: angular.material.IMedia,
        private configService: ConfigService,
        private authManager: AuthManager) {

        this.$log.debug(`${MainComponent.name}::ctor`);
    }

    /**
     * Inicia el componente.
     * @memberof MainComponent
     */
    public $onInit(): void {

        this.$log.debug(`${MainComponent.name}::$onInit`);

        this.model = {
            id: 'leftPanel',
            user: <IAuthUserModel>{},
            version: this.configService.getVersion()
        };

        this.authManager.getCurrentUser().then((user: IAuthUserModel) => {
            this.model.user = user;
        });

        this.viewData = {
            isOpen: this.$mdMedia('gt-sm'),
            isLocked: this.$mdMedia('gt-sm'),
            states: {
                login: LoginState.login,
                main: WelcomeState.welcome,
                changePassword: ChangePasswordState.changePassword,
                automatization: AutomatizationState.automatization
            }
        };
    }

    /**
     * Toggle de sideNav desde el ícono de menú.
     * @memberof MainComponent
     */
    public toggleLeftMenu(): void {
        if (this.$mdMedia('gt-sm')) {
            this.viewData.isLocked = !this.viewData.isLocked;
        } else {
            this.viewData.isOpen = !this.viewData.isOpen;
        }
    }

    /**
     * Devuelve verdadero si hay que mostrar el menu izquierdo.
     * Falso en caso contrario.
     * @returns {boolean} verdadero si hay que mostrar el menu izquierdo. 
     * Falso en caso contrario.
     * @memberof MainComponent
     */
    public isLocked(): boolean {
        const isMediaGreaterSmall: boolean = this.$mdMedia('gt-sm');
        if (isMediaGreaterSmall && this.viewData.isOpen) {
            this.viewData.isOpen = false;
        }
        return isMediaGreaterSmall && this.viewData.isLocked;
    }

    /**
     * Esconde el sidenav cuando se produce un cambio de state.
     * @memberof MainComponent
     */
    public changeState(): void {
        const sideNav: angular.material.ISidenavObject = this.$mdSidenav(this.model.id);
        if (!this.$mdMedia('gt-sm') && sideNav.isOpen()) {
            sideNav.close();
        }
    }

    /**
     * Cierra la sesion y cambia al state login.
     * @memberof MainComponent
     */
    public logout(): void {
        this.authManager.logout();
        this.changeState();
    }
}