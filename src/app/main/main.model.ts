'use strict';

import { IAuthUserModel } from '../auth/auth-user.model';

/**
 * Modelo de la vista main.
 * @export
 * @interface IMainModel
 */
export interface IMainModel {

    /**
     * Id.
     * @type {string}
     */
    id: string;
    user: IAuthUserModel;
    version:string;
}