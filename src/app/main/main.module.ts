'use strict';

import angular from 'angular';
import 'angular-localforage';
import 'angular-animate';
import 'angularMoment';
import 'angular-jwt';
import { NgModule, ModuleDecorated } from 'angular-ts-decorators';
import { ModuleHelper } from '../../core/module.helper';
import { ApiModule } from '../../api/api.module';
import { AuthModule } from '../auth/auth.module';
import { MainComponent } from './main.component';

/**
 * Módulo para Main.
 * @preserve
 */
@NgModule({
    declarations: [MainComponent],
    providers: [],
    imports: [
        'ngAnimate',
        ApiModule.name,
        AuthModule.name
    ]
})
export class MainModule implements ModuleDecorated {

    /**
     * Configuración del módulo.
     */
    public config(): void {
        'ngInject';
        const $log: angular.ILogService = ModuleHelper.getLogger();
        $log.debug(`${MainModule.name}::config`);
    }

    /**
     * Corre el módulo.
     */
    public run(): void {
        'ngInject';
        const $log: angular.ILogService = ModuleHelper.getLogger();
        $log.debug(`${MainModule.name}::run`);
    }
}