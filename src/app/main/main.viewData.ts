'use strict';

/**
 * ViewData para la vista main.
 * @export
 * @interface IMainViewData
 */
export interface IMainViewData {
    isOpen: boolean;
    isLocked: boolean;
    // tslint:disable-next-line:no-any
    states: {[key: string]: any};
}