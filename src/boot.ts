'use strict';

import 'tslib';
import angular from 'angular';
import { AppModule } from './app/app.module';
import { ModuleHelper } from './core/module.helper';
import 'material-icons.css';

// Inicia la aplicación angularjs.
angular.element(document).ready(() => {
    // Asigna el logger custom.
    const $log: angular.ILogService = ModuleHelper.getLogger();
    
    $log.debug(`angular.bootstrap: ${AppModule.name}`);

    // HACK: 2017-07-30 - (FAS / DAE) - Los md-input-container se visualizan mal con la carga asincronica.
    // Link: https://github.com/angular/material/issues/5318
    angular.element(document.body).append('<md-input-container style="display: none;"><input /></md-input-container>');
    
    // Inicia la aplicación.
    const htmlNode: HTMLElement = document.querySelector('html');
    angular.bootstrap(htmlNode, [AppModule.name]);
});