export interface IMoment {

    (inp?: IMomentInput, format?: IMomentFormatSpecification, strict?: boolean): IMoment;
    (inp?: IMomentInput, format?: IMomentFormatSpecification, language?: string, strict?: boolean): IMoment;
    add(amount?: DurationInputArg1, unit?: DurationInputArg2): IMoment;
    subtract(amount?: DurationInputArg1, unit?: DurationInputArg2): Date;
    format(format?: string): Date;

    subtract(unit: unitOfTime.DurationConstructor, amount: number | string): Date;
    toISOString(): string;
    year(): string;
    duration(inp?: DurationInputArg1, unit?: DurationInputArg2): Duration;
    locale(): string;
    hours(h: number): IMoment;

    minute(m: number): IMoment;
    minute(): number;
    minutes(m: number): IMoment;
    minutes(): number;
    second(s: number): IMoment;
    second(): number;
    seconds(s: number): IMoment;
    seconds(): number;
    hour(h: number): IMoment;
    hour(): number;
    hours(h: number): IMoment;
    hours(): number;
    millisecond(ms: number): IMoment;
    millisecond(): number;
    milliseconds(ms: number): IMoment;
    milliseconds(): number;

    toDate(): Date;
}

export interface FromTo {

    // tslint:disable-next-line:no-reserved-keywords Se deshabilita ya que es una variable de la libreria.
    from: IMomentInput;
    to: IMomentInput;
}
export interface IMomentInputObject {
    years?: number;
    year?: number;
    y?: number;

    months?: number;
    month?: number;
    M?: number;

    days?: number;
    day?: number;
    d?: number;

    dates?: number;
    date?: number;
    D?: number;

    hours?: number;
    hour?: number;
    h?: number;

    minutes?: number;
    minute?: number;
    m?: number;

    seconds?: number;
    second?: number;
    s?: number;

    milliseconds?: number;
    millisecond?: number;
    ms?: number;

    // tslint:disable-next-line:no-any Se deshabilita ya que no se sabe el tipo de dato.
    format(): any;
}

export interface Duration {
    humanize(withSuffix?: boolean): string;

    abs(): Duration;

    as(units: unitOfTime.Base): number;
    get(units: unitOfTime.Base): number;

    milliseconds(): number;
    asMilliseconds(): number;

    seconds(): number;
    asSeconds(): number;

    minutes(): number;
    asMinutes(): number;

    hours(): number;
    asHours(): number;

    days(): number;
    asDays(): number;

    weeks(): number;
    asWeeks(): number;

    months(): number;
    asMonths(): number;

    years(): number;
    asYears(): number;

    add(inp?: DurationInputArg1, unit?: DurationInputArg2): Duration;
    subtract(inp?: DurationInputArg1, unit?: DurationInputArg2): Date;

    locale(): string;
    locale(locale: LocaleSpecifier): Duration;
    localeData(): Locale;

    toISOString(): string;
    toJSON(): string;

    /**
     * @deprecated since version 2.8.0
     */
    lang(locale: LocaleSpecifier): IMoment;
    /**
     * @deprecated since version 2.8.0
     */
    lang(): Locale;
    /**
     * @deprecated
     */
    toIsoString(): string;

    format(value: string): string;
}

export interface Locale {
    calendar(key?: CalendarKey, m?: IMoment, now?: IMoment): string;

    longDateFormat(key: LongDateFormatKey): string;
    invalidDate(): string;
    ordinal(n: number): string;

    preparse(inp: string): string;
    postformat(inp: string): string;
    relativeTime(n: number, withoutSuffix: boolean, key: RelativeTimeKey, isFuture: boolean): string;
    pastFuture(diff: number, absRelTime: string): string;
    set(config: Object): void;

    months(): string[];
    months(m: IMoment, format?: string): string;
    monthsShort(): string[];
    monthsShort(m: IMoment, format?: string): string;
    monthsParse(monthName: string, format: string, strict: boolean): number;
    monthsRegex(strict: boolean): RegExp;
    monthsShortRegex(strict: boolean): RegExp;

    week(m: IMoment): number;
    firstDayOfYear(): number;
    firstDayOfWeek(): number;

    weekdays(): string[];
    weekdays(m: IMoment, format?: string): string;
    weekdaysMin(): string[];
    weekdaysMin(m: IMoment): string;
    weekdaysShort(): string[];
    weekdaysShort(m: IMoment): string;
    weekdaysParse(weekdayName: string, format: string, strict: boolean): number;
    weekdaysRegex(strict: boolean): RegExp;
    weekdaysShortRegex(strict: boolean): RegExp;
    weekdaysMinRegex(strict: boolean): RegExp;

    isPM(input: string): boolean;
    meridiem(hour: number, minute: number, isLower: boolean): string;
}
export type IBase = (
    'year' | 'years' | 'y' |
    'month' | 'months' | 'M' |
    'week' | 'weeks' | 'w' |
    'day' | 'days' | 'd' |
    'hour' | 'hours' | 'h' |
    'minute' | 'minutes' | 'm' |
    'second' | 'seconds' | 's' |
    'millisecond' | 'milliseconds' | 'ms'
);


export namespace unitOfTime {
    type Base = IBase;
    // tslint:disable-next-line:no-shadowed-variable Se deshabilita ya que son variables que expone la libreria.    
    type _quarter = 'quarter' | 'quarters' | 'Q';
    type _isoWeek = 'isoWeek' | 'isoWeeks' | 'W';
    type _date = 'date' | 'dates' | 'D';
    // tslint:disable-next-line:no-shadowed-variable Se deshabilita ya que son variables que expone la libreria.  
    type DurationConstructor = Base | _quarter;

    type DurationAs = Base;

    type StartOf = Base | _quarter | _isoWeek | _date;

    type Diff = Base | _quarter;

    type MomentConstructor = Base | _date;

    type All = Base | _quarter | _isoWeek | _date |
        'weekYear' | 'weekYears' | 'gg' |
        'isoWeekYear' | 'isoWeekYears' | 'GG' |
        'dayOfYear' | 'dayOfYears' | 'DDD' |
        'weekday' | 'weekdays' | 'e' |
        'isoWeekday' | 'isoWeekdays' | 'E';
}
export interface DurationInputObject extends MomentInputObject {
    quarters?: number;
    quarter?: number;
    Q?: number;

    weeks?: number;
    week?: number;
    w?: number;
}
export interface MomentInputObject {
    years?: number;
    year?: number;
    y?: number;

    months?: number;
    month?: number;
    M?: number;

    days?: number;
    day?: number;
    d?: number;

    dates?: number;
    date?: number;
    D?: number;

    hours?: number;
    hour?: number;
    h?: number;

    minutes?: number;
    minute?: number;
    m?: number;

    seconds?: number;
    second?: number;
    s?: number;

    milliseconds?: number;
    millisecond?: number;
    ms?: number;
}

export type IMomentFormatSpecification = string | IMomentBuiltinFormat | (string | IMomentBuiltinFormat)[];

export interface IMomentBuiltinFormat {
    // tslint:disable-next-line:no-any Se deshabilita ya que no se sabe el tipo de dato.
    __momentBuiltinFormatBrand: any;
}

export type IMomentInput = IMoment | Date | string | number | (number | string)[] | IMomentInputObject | void; // Type: null | Undefined
export type DurationConstructor = IBase | _quarter;
export type RelativeTimeKey = 's' | 'm' | 'mm' | 'h' | 'hh' | 'd' | 'dd' | 'M' | 'MM' | 'y' | 'yy';
export type LongDateFormatKey = 'LTS' | 'LT' | 'L' | 'LL' | 'LLL' | 'LLLL' | 'lts' | 'lt' | 'l' | 'll' | 'lll' | 'llll';
export type CalendarKey = 'sameDay' | 'nextDay' | 'lastDay' | 'nextWeek' | 'lastWeek' | 'sameElse' | string;
export type _quarter = 'quarter' | 'quarters' | 'Q';
export type DurationInputArg1 = Duration | number | string | FromTo | DurationInputObject | void; // Type: null | undefined
export type DurationInputArg2 = unitOfTime.DurationConstructor;
export type LocaleSpecifier = string | IMoment | Duration | string[] | boolean;