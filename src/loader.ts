// tslint:disable:semicolon
// tslint:disable:no-unnecessary-semicolons
// tslint:disable:only-arrow-functions
// tslint:disable:no-function-expression
// tslint:disable:no-unused-expression

// tslint:disable-next-line:no-extra-semi
;
/*
 * Importa boot.
 */
(function(sys: SystemJSLoader.System): void {
  'use strict';

  const methodName : string = 'loader::System.import';
  console && console.log(`${methodName}: boot`);

  sys.import('boot')
    .then(function(): void {
      console && console.log(`${methodName} (then) %o`, arguments);
    })
    .catch(function():void {
      console && console.error(`${methodName} (catch) %o`, arguments);
    });
})(System);