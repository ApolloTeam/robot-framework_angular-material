;
(function () {
    'use strict';

    var config = {
        baseURL: './',
        defaultExtension: 'js',
        paths: {
            '/': '/'
        },
        map: {
            'angular': 'vendor/angular/angular.js',
            'angular-animate': 'vendor/angular-animate/angular-animate.js',
            'angular-material': 'vendor/angular-material/angular-material.js',
            'angular-material-icons': 'vendor/angular-material-icons/angular-material-icons.min.js',
            'angular-aria': 'vendor/angular-aria/angular-aria.js',
            'angular-messages': 'vendor/angular-messages/angular-messages.js',
            '@uirouter/angularjs':'vendor/@uirouter/angular-ui-router.js',
            'angular-ts-decorators': 'vendor/angular-ts-decorators/angular-ts-decorators.js',
            'angular-translate': 'vendor/angular-translate/angular-translate.js',
            'angular-translate-loader-partial': 'vendor/angular-translate-loader-partial/angular-translate-loader-partial.js',
            'angular-translate-loader-static-files': 'vendor/angular-translate-loader-static-files/angular-translate-loader-static-files.js',
            'angular-mocks': 'vendor/angular-mocks/angular-mocks.js',
            'angular-jwt': 'vendor/angular-jwt/angular-jwt.js',
            'angular-localforage': 'vendor/angular-localforage/angular-localForage.js',
            'localforage': 'vendor/localforage/localforage.js',
            'reflect-metadata': 'vendor/reflect-metadata/Reflect.js',
            'oclazyload': 'vendor/oclazyload/ocLazyLoad.js',
            'tslib': 'vendor/tslib/tslib.js',
            'css': 'vendor/plugin-css/css.js',
            'material-icons.css': 'vendor/material-design-icons/material-icons.css',
            'angularMoment': 'vendor/angular-moment/angular-moment.js',
            'moment': 'vendor/moment/moment.js',
            'moment-duration-format': 'vendor/moment-duration-format/moment-duration-format.js',
            'angular1-zones': 'vendor/angular1-zones/angular1-zones.js',
            'zone': 'vendor/zone/zone.js'
        },
        meta: {
            'vendor/angular/angular.js': {
                format: 'global',
                exports: 'angular'
            },
            'vendor/angular-material/angular-material.js': {
                deps: [
                    'angular',
                    'angular-animate',
                    'angular-aria',
                    'angular-messages'
                ]
            },
            'vendor/angular-material-icons/angular-material-icons.min.js': {
                deps: [
                    'angular-material',
                    'vendor/angular-material-icons/angular-material-icons.css'
                ]
            },
            'vendor/angular-translate-loader-static-files/angular-translate-loader-static-files.js': {
                deps: ['angular-translate']
            },
            'vendor/angular-translate-loader-partial/angular-translate-loader-partial.js': {
                deps: ['angular-translate']
            },
            'vendor/angular-translate/angular-translate.js': {
                deps: ['angular']
            },
            'vendor/angular-ui-router/angular-ui-router.js': {
                deps: ['angular']
            },
            'vendor/angular-messages/angular-messages.js': {
                deps: ['angular']
            },
            'vendor/oclazyload/oclazyload.js': {
                deps: ['angular']
            },
            'vendor/angular1-zones/angular1-zones.js': {
                deps: [
                    'zone',
                    'angular'
                ]
            },
            '*.css': { loader: 'css' }
        },
        packages: {
            '/': {
                defaultExtension: 'js'
            }
        }
    };

    System.config(config);
}());