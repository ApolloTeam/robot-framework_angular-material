// tslint:disable:no-any

/**
 * Constantes para códigos de error de Excepciones y StatusMessage (api).
 * @export
 * @enum {number}
 */
export enum ApiErrorCodes {
    /** Only grant_type=password and refresh_token requests are accepted by this server. */
    ErrAuthUnsupportedGrantType = <any>'ERR.AUTH.UNSUPPORTED_GRANT_TYPE',

    /** AccessToken inválido. */
    ErrAuthAccessTokenInvalid = <any>'ERR.AUTH.ACCESSTOKEN.INVALID',

    /** AccessToken vencido. */
    ErrAuthAccessTokenExpired = <any>'ERR.AUTH.ACCESSTOKEN.EXPIRED',

    /** AccessToken con cantidad de usos (hits) superada. */
    ErrAuthAccessTokenOverHitsLimit = <any>'ERR.AUTH.ACCESSTOKEN.OVERHITSLIMIT',

    /** Valor de llave AppKey inválida (no encontrada).  */
    ErrAuthInvalidAppKey = <any>'ERR.AUTH.INVALIDAPPKEY',

    /** Credenciales inválidas. */
    ErrAuthInvalidCredentials = <any>'ERR.AUTH.INVALIDCREDENTIALS',

    /** Cuenta de Empresa o Pasajero inactiva. */
    ErrAuthInanctiveAccount = <any>'ERR.AUTH.INANCTIVEACCOUNT',

    /** Pasajero sin privilegio de login. */
    ErrAuthNoLoginPrivilege = <any>'ERR.AUTH.NOLOGINPRIVILEGE',

    /** The Authorization header is missing or has an invalid format (format: Authorization:Bearer {yourToken}) */
    ErrAuthAuthorizationHeader = <any>'ERR.AUTH.AUTHORIZATIONHEADER',

    /** La nueva password es distinta a la confirmación. */
    ErrAuthNewPasswordNotmatchConfirmation = <any>'ERR.AUTH.NEWPASSWORD.NOTMATCH.CONFIRMATION',

    /** La nueva password no cumple con las políticas de seguridad. */
    ErrAuthNewPasswordTooweak = <any>'ERR.AUTH.NEWPASSWORD.TOOWEAK',

    /** Invalid or expired token on reset password. */
    ErrAuthResetPasswordInvalidOrExpiredToken = <any>'ERR.AUTH.RESETPASSWORD_INVALIDOREXPIREDTOKEN',

    /** No response from API. */  
    ErrNoResponseFromApi = <any>'ERR.NORESPONSEFROMAPI',

    /** Para InternalServerError. */ 
    ErrInternalServerError = <any>'ERR',

    /** Para validación sin ErrorCode específico. */ 
    ErrBadRequest = <any>'ERR.VALIDATION'
}