'use strict';

/**
 * Reset password request.
 * @export
 * @interface IPasswordResetRequest
 */
export interface IPasswordResetRequest {
    
    /**
     * Token to authenticate the user.
     * @type {string}
     */
    token: string;

    /**
     * New password
     * @type {string}
     */
    newPassword: string;

    /**
     * Confirmation for the new password.
     * @type {string}
     */
    confirmation: string;
}