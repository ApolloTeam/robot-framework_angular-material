'use strict';

/**
 * Login request.
 * @export
 * @interface ILoginRequest
 */
export interface ILoginRequest {

    /**
     * User name.
     * @type {string}
     * @memberof ILoginRequest
     */
    username: string;

    /**
     * The user password.
     * @type {string}
     * @memberof ILoginRequest
     */
    password: string;

    /**
     * Client Identifier valid at the Authorization Server (the AppKey).
     * @type {string}
     * @memberof ILoginRequest
     */
    client_id: string;
    
    /**
     * Grant type. The value must be "password".
     * @type {string}
     * @memberof ILoginRequest
     */
    grant_type: string;

    /**
     * The scopes.
     * @type {string}
     * @memberof ILoginRequest
     */
    scopes: string;
}