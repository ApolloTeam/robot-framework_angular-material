'use strict';

import { NgModule, ModuleDecorated } from 'angular-ts-decorators';
import { ModuleHelper } from '../../core/module.helper';
import { CoreModule } from '../../core/core.module';
import { AuthApiService } from './auth-api.service';

/**
 * Declara el módulo para Auth.
 * @export
 * @class AuthApiModule
 * @implements {ModuleDecorated}
 */
@NgModule({
    declarations: [],
    providers: [AuthApiService],
    imports: [
        CoreModule
    ]
})
export class AuthApiModule
    implements ModuleDecorated {

    /**
     * Configuración del módulo.
     */
    public config(): void {
        'ngInject';
        const $log: angular.ILogService = ModuleHelper.getLogger();
        $log.debug(`${AuthApiModule.name}::config`);
    }

    /**
     * Corre el módulo.
     */
    public run(): void {
        'ngInject';
        const $log: angular.ILogService = ModuleHelper.getLogger();
        $log.debug(`${AuthApiModule.name}::run`);
    }
}