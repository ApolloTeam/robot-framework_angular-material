'use strict';

import angular from 'angular';
import { Injectable } from 'angular-ts-decorators';
import { ConfigService } from '../../app/app-config.service';
import { HttpHelperService } from '../../core/http-helper.service';
import { IStatusMessage } from '../../core/entities/status-message.entity';
import { IPromiseCancelable } from '../../core/entities/promise-cancelable';
import { IHttpPromiseCancelable } from '../../core/entities/http-promise-cancelable';
import { IPasswordResetRequest } from './password-reset-request.entity';
import { ILoginRequest } from './login-request.entity';
import { IChangePasswordRequest } from './change-password-request.entity';
import { IJsonWebToken } from './json-web-token';

/**
 * Operaciones del módulo Auth.
 * @class AuthApiService
 */
@Injectable()
export class AuthApiService {

    /**
     * Declaración de dependencias.
     * @static
     * @memberOf AuthApiService
     */
    public static $inject = [
        '$log',
        '$httpParamSerializerJQLike',
        ConfigService.name,
        HttpHelperService.name
    ];

    /**
     * Creates an instance of AuthApiService.
     * @param {angular.ILogService} $log Servicio de logs.
     * @param {angular.IHttpParamSerializer} $httpParamSerializerJQLike Permite la serealizacion de la data.
     * @param {IConfigService} configService Permite acceder a las urls que conectan con el api.
     * @param {IHttpHelperService} httpHelperService Permite interceptar la llamada al api para su modificación.
     * @memberOf AuthApiService
     */
    constructor(
        private $log: angular.ILogService,
        private $httpParamSerializerJQLike: angular.IHttpParamSerializer,
        private configService: ConfigService,
        private httpHelperService: HttpHelperService) {

        this.$log.debug(`${AuthApiService.name}::ctor`);
    }

    /**
     * Reset the password corresponding to the user asociated on the token.
     * @param {IPasswordResetRequest} passwordResetRequest Reset password request.
     * @returns {(IPromiseCancelable<void | IStatusMessage>)} Promesa cancelable con la respuesta.
     */
    public resetPassword(passwordResetRequest: IPasswordResetRequest): IPromiseCancelable<void | IStatusMessage> {
        const methodName: string = `${AuthApiService.name}::resetPassword`;
        this.$log.debug(methodName);

        const url: string = this.configService.getAuthUrl() + '/auth/resetpassword';
        const appkeyValue: string = this.configService.getAppKey();

        const config: angular.IRequestConfig = {
            url: url,
            method: 'POST',
            data: passwordResetRequest,
            headers: {
                'Authorization': `AppKey ${appkeyValue}`
            }
        };

        return this.httpHelperService.makeCancelable<void | IStatusMessage>(methodName, config);
    }


    /**
     * Request an OpenId Authentication.
     * @param {ILoginRequest} loginRequest Login request.
     * @returns {(IPromiseCancelable<void | IStatusMessage>)} Promesa cancelable con la respuesta.
     * @memberof AuthApiService
     */
    public login(loginRequest: ILoginRequest): IPromiseCancelable<IJsonWebToken | IStatusMessage> {
        const methodName: string = `${AuthApiService.name}::login`;
        this.$log.debug(methodName);

        const url: string = this.configService.getAuthUrl() + '/connect/token';
        const appkeyValue: string = this.configService.getAppKey();
        const grandTypeValue: string = this.configService.getGrandType();

        loginRequest.client_id = appkeyValue;
        loginRequest.grant_type = grandTypeValue;
        loginRequest.scopes = '';

        const config: angular.IRequestConfig = {
            url: url,
            method: 'POST',
            data: this.$httpParamSerializerJQLike(loginRequest),
            headers: {
                'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'
            }
        };

        return this.httpHelperService.makeCancelable<IJsonWebToken | IStatusMessage>(methodName, config);
    }

    /**
     * Request a password reset.
     * @param {string} username Nombre o email del usuario.
     * @returns {(IPromiseCancelable<void | IStatusMessage>)} Promesa cancelable con la respuesta.
     * @memberof AuthApiService
     */
    public passwordResetRequest(username: string): IPromiseCancelable<void | IStatusMessage> {
        const methodName: string = `${AuthApiService.name}::passwordResetRequest`;
        this.$log.debug(methodName);

        const url: string = this.configService.getAuthUrl() + `/auth/${username}/passwordresetrequest`;
        const appkeyValue: string = this.configService.getAppKey();

        const config: angular.IRequestConfig = {
            url: url,
            method: 'POST',
            headers: {
                'Authorization': `AppKey ${appkeyValue}`
            }
        };

        return this.httpHelperService.makeCancelable<void | IStatusMessage>(methodName, config);
    }

    /**
     * Change the password of the authenticated user.
     * @param {IChangePasswordRequest} changePasswordRequest change password request.
     * @returns {(IPromiseCancelable<void | IStatusMessage>)} Promesa cancelable con la respuesta.
     * @memberof AuthApiService
     */
    public changePassword(changePasswordRequest: IChangePasswordRequest): IPromiseCancelable<void | IStatusMessage> {
        const methodName: string = `${AuthApiService.name}::changePassword`;
        this.$log.debug(methodName);

        const url: string = this.configService.getAuthUrl() + '/auth/password';
        const appkeyValue: string = this.configService.getAppKey();
        
        const config: angular.IRequestConfig = {
            url: url,
            method: 'PUT',
            data:changePasswordRequest
        };

        return this.httpHelperService.makeCancelable<void | IStatusMessage>(methodName, config);
    }

    public searchClients(searchText: string): IPromiseCancelable<string[]> {
        const methodName: string = `${AuthApiService.name}::searchClients`;
        this.$log.debug(methodName);

        const url: string = this.configService.getAuthUrl() + `/auth/clients/${searchText}`;
        
        const config: angular.IRequestConfig = {
            url: url,
            method: 'GET'
        };

        return this.httpHelperService.makeCancelable<string[]>(methodName, config);
    }
} 