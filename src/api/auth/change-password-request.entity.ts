'use strict';

/**
 * Change password request.
 * @export
 * @interface IChangePasswordRequest
 */
export interface IChangePasswordRequest {

    /**
     * Old password.
     * @type {string}
     * @memberof IChangePasswordRequest
     */
    oldPassword: string;

    /**
     * New password.
     * @type {string}
     * @memberof IChangePasswordRequest
     */
    newPassword: string;

    /**
     * Confirmation password.
     * @type {string}
     * @memberof IChangePasswordRequest
     */
    confirmation: string;
}