'use strict';

import { NgModule, ModuleDecorated } from 'angular-ts-decorators';
import { ApiService } from './api.service';
import { AuthApiModule } from './auth/auth-api.module';
import { ModuleHelper } from '../core/module.helper';

/**
 * Declara el módulo de para las APIs.
 * @export
 * @preserve
 * @class ApiModule
 * @implements {ModuleDecorated}
 */
@NgModule({
    declarations: [],
    providers: [ApiService],
    imports: [
        AuthApiModule
    ]
})
export class ApiModule implements ModuleDecorated {
    
    /**
     * Configuración del módulo.
     */
    public config(): void {
        'ngInject';
        const $log: angular.ILogService = ModuleHelper.getLogger();
        $log.debug(`${ApiModule.name}::config`);
    }

    /**
     * Corre el módulo.
     */
    public run(): void {
        'ngInject';
        const $log: angular.ILogService = ModuleHelper.getLogger();
        $log.debug(`${ApiModule.name}::config`);
    }
}