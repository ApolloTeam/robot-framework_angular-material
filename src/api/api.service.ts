'use strict';

import angular from 'angular';
import { Injectable } from 'angular-ts-decorators';
import { AuthApiService } from './auth/auth-api.service';

/**
 * Brinda acceso a los modulos del API.
 * @export
 * @class ApiService
 */
@Injectable()
export class ApiService {

    /**
     * Declaración de dependencias.
     * @static
     * @memberOf ApiService
     */
    public static $inject = [
        '$log',
        AuthApiService.name
    ];
    
    /**
     * Creates an instance of ApiService.
     * @param {angular.ILogService} $log Servicio de logs.
     * @param {AuthService} authService Servico de auth.
     */
    constructor(
        private $log: angular.ILogService,
        private authService: AuthApiService
        ) {
        this.$log.debug(`${ApiService.name}::ctor`);
    }

    /**
     * Devuelve una instancia de la clase Auth.
     * @returns {AuthService} Una instancia de la clase Auth.
     * @memberOf ApiService
     */
    public auth(): AuthApiService {
        return this.authService;
    }
}