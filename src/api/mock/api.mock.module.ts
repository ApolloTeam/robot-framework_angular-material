'use strict';

import angular from 'angular';
import 'angular-mocks';

import { NgModule, ModuleDecorated } from 'angular-ts-decorators';
import { ModuleHelper } from '../../core/module.helper';
import { ApiErrorCodes } from '../commons/api-error-codes';
import { IStatusMessage } from '../../core/entities/status-message.entity';
import { AuthMockModule } from './auth/auth-mock.module';

/**
 * Módulo para mockear respuestas HTTP.
 * @export
 * @class ApiMockModule
 * @implements {ModuleDecorated}
 */
@NgModule({
    declarations: [],
    providers: [],
    imports: [
        'ngMockE2E',
        AuthMockModule.name
    ]
})
export class ApiMockModule implements ModuleDecorated {

    /**
     * Configuración del módulo.
     * @memberof ApiMockModule
     */
    public config(): void {
        'ngInjectls';

        const $log: angular.ILogService = ModuleHelper.getLogger();
        $log.debug(`${ApiMockModule.name}::config`);
    }

    /**
     * Corre el módulo.
     * @param {angular.IHttpBackendService} $httpBackend Servicio para configurar endpoints y mockear su response.
     * @memberof ApiMockModule
     */
    public run($httpBackend: angular.IHttpBackendService): void {
        'ngInject';

        const $log: angular.ILogService = ModuleHelper.getLogger();
        $log.debug(`${ApiMockModule.name}::run`);

        $httpBackend.whenGET(/.*\.(html|json)/i).passThrough();
    }
}