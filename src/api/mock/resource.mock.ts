// tslint:disable:no-any

'use strict';

import angular from 'angular';
import { Injectable } from 'angular-ts-decorators';

/**
 * Arma mock de respuestas HTTP.
 * @export
 * @class ResourceMock
 */
export abstract class ResourceMock {

    /**
     * Creates an instance of ResourceMock.
     * @param {angular.ILogService} $log Servicio de logs.
     * @memberOf ResourceMock
     */
    constructor(
        protected $log: angular.ILogService) {

        this.$log.debug(`${ResourceMock.name}::ctor`);
    }
    /**
     * Devuelve una respuesta mockeada con status code http 200 (OK).
     * @protected
     * @param {*} data Recibe any para enviarlo en el response.
     * @returns {any[]} Response http.
     * @memberof ResourceMock
     */
    protected ok(data: any): any[] {
        return [200, data, {}];
    }

    /**
     * Devuelve una respuesta mockeada con status code http 204 (No Content).
     * @protected
     * @returns {any[]} Response http.
     * @memberof ResourceMock
     */
    protected noContent(): any[] {
        return [204, null, {}];
    }

    /**
     * Devuelve una respuesta mockeada status code http 400 (Bad Request).
     * @protected
     * @param {*} data Recibe any para enviarlo en el response.
     * @returns {any[]} Response http.
     * @memberof ResourceMock
     */
    protected badRequest(data: any): any[] {
        return [400, data, {}];
    }

    /**
     * Devuelve una respuesta mockeada con status code http 401 (Unauthorized).
     * @protected
     * @param {*} data Recibe any para enviarlo en el response.
     * @returns {any[]} Response http.
     * @memberof ResourceMock
     */
    protected unauthorized(data: any): any[] {
        return [401, data, {}];
    }

    /**
     * Devuelve una respuesta mockeada con status code http 403 (Forbidden).
     * @protected
     * @param {*} data Recibe any para enviarlo en el response.
     * @returns {any[]} Response http.
     * @memberof ResourceMock
     */
    protected forbidden(data: any): any[] {
        return [403, data, {}];
    }

    /**
     * Devuelve una respuesta mockeada con status code http 404 (Not found).
     * @protected
     * @param {*} data Recibe any para enviarlo en el response.
     * @returns {any[]} Response http.
     * @memberof ResourceMock
     */
    protected notFound(data: any): any[] {
        return [404, data, {}];
    }

    /**
     * Castea el data del request.
     * @protected
     * @template TEntity Entidad para castear el modelo.
     * @param {string} data Data del request.
     * @param {*} [headers=null] headers del request.
     * @returns {TEntity} Cast del data del request.
     * @memberof ResourceMock
     */
    protected getData<TEntity>(data: string, headers: any = null): TEntity {
        let ret: TEntity;
        if (headers && headers['Content-Type'] && headers['Content-Type'].indexOf('x-www-form-urlencoded') > -1) {

            ret = data.split('&').reduce((acc, cur) => {
                const curSplit: string[] = cur.split('=');
                const key: string = curSplit[0];
                const value: string = curSplit[1];
                acc[key] = value;
                return acc;
            },                           <TEntity>{});
        } else {
            ret = <TEntity>angular.fromJson(data);
        }

        return ret;
    }
}