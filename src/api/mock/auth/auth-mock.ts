// tslint:disable:no-any

'use strict';

import angular from 'angular';
import { ResourceMock } from '../resource.mock';
import { ApiErrorCodes } from '../../commons/api-error-codes';
import { IStatusMessage } from '../../../core/entities/status-message.entity';
import { IJsonWebToken } from '../../auth/json-web-token';
import { ILoginRequest } from '../../auth/login-request.entity';
import { IPasswordResetRequest } from '../../auth/password-reset-request.entity';
import { IChangePasswordRequest } from '../../auth/change-password-request.entity';

/**
 * Crea mocks para simular llamadas al API del móduo Auth.
 * @class AuthMock
 */
export class AuthMock extends ResourceMock {

    /**
     * Declaración de dependencias.
     * @static
     * @type {string[]}
     */
    public static $inject: string[] = [
        '$log'
    ];

    /**
     * Creates an instance of AuthMock.
     * @param {angular.ILogService} $log Servicio de logs.
     * @memberof AuthMock
     */
    constructor($log: angular.ILogService) {
        super($log);
        $log.debug(`${AuthMock.name}::ctor`);
    }

    /**
     * Devuevlve un mock para request de POST /auth/resetpassword.
     * @param {string} method Nombre de verbo http.
     * @param {string} url URL completa del endpoint.
     * @param {*} data Objeto que se envía en el campo data del request (body).
     * @param {*} headers Objeto que se envía en el campo headers del request.
     * @returns {any[]} HTTP Response.
     * @memberof AuthMock
     */
    public postAuthResetPassword(method: string, url: string, data: any, headers: any): any[] {
        this.$log.debug(`${AuthMock.name}::postAuthResetPassword method %o, url %o, data %o, headers %o`, method, url, data, headers);

        const payload: IPasswordResetRequest = this.getData<IPasswordResetRequest>(data);
        const date: Date = new Date();
        let statusMessage: IStatusMessage;


        switch (payload.newPassword) {

            case 'EANNC_':
                // Mock para confirmación de contraseña inválida.
                statusMessage = {
                    code: '400',
                    descrip: 'BadRequest',
                    errorCode: <any>ApiErrorCodes.ErrAuthNewPasswordNotmatchConfirmation,
                    errorUniqueId: 'aa3caf5fca8f4742b138fbddbd06f464',
                    message: null,
                    timeStamp: date.toISOString(),
                    validationErrors: null
                };
                break;

            case 'EANT__':
                // Mock para password insegura.
                statusMessage = {
                    code: '400',
                    descrip: 'BadRequest',
                    errorCode: <any>ApiErrorCodes.ErrAuthNewPasswordTooweak,
                    errorUniqueId: 'aa3caf5fca8f4742b138fbddbd06f464',
                    message: null,
                    timeStamp: date.toISOString(),
                    validationErrors: null
                };
                break;

            case 'EARI__':
                // Mock para token inválido o expirado.
                statusMessage = {
                    code: '401',
                    descrip: 'Unauthorized',
                    errorCode: <any>ApiErrorCodes.ErrAuthResetPasswordInvalidOrExpiredToken,
                    errorUniqueId: 'aa3caf5fca8f4742b138fbddbd06f464',
                    message: null,
                    timeStamp: date.toISOString(),
                    validationErrors: null
                };
                break;

            case '500___':
                // Mock para error genérico.
                statusMessage = {
                    code: '500',
                    descrip: 'InternalServerError',
                    errorCode: <any>ApiErrorCodes.ErrInternalServerError,
                    errorUniqueId: 'aa3caf5fca8f4742b138fbddbd06f464',
                    message: null,
                    timeStamp: date.toISOString(),
                    validationErrors: null
                };
                break;

            case '502___':
                // Mock para No response from external API.
                statusMessage = {
                    code: '502',
                    descrip: 'BadGateway',
                    errorCode: <any>ApiErrorCodes.ErrNoResponseFromApi,
                    errorUniqueId: 'aa3caf5fca8f4742b138fbddbd06f464',
                    message: null,
                    timeStamp: date.toISOString(),
                    validationErrors: null
                };
                break;

            default:
                break;
        }

        return statusMessage ? [statusMessage.code, statusMessage, {}] : this.noContent();
    }

    /**
     * Devuevlve un mock para request de POST /auth/resetpassword.
     * @param {string} method Nombre de verbo http.
     * @param {string} url URL completa del endpoint.
     * @param {*} data Objeto que se envía en el campo data del request (body).
     * @param {*} headers Objeto que se envía en el campo headers del request.
     * @returns {any[]} HTTP Response.
     * @memberof AuthMock
     */
    // tslint:disable-next-line:max-func-body-length
    public postAuthLogin(method: string, url: string, data: any, headers: any): any[] {
        this.$log.debug(`${AuthMock.name}::postAuthLogin method %o, url %o, data %o, headers %o`, method, url, data, headers);

        const payload: ILoginRequest = this.getData<ILoginRequest>(data, headers);

        const date: Date = new Date();
        let statusMessage: IStatusMessage;
        let jwtSuccess: IJsonWebToken;

        switch (payload.username) {
            case '400':
                // Requested invalid or incomplete.
                statusMessage = {
                    code: '400',
                    descrip: 'BadRequest',
                    errorCode: <any>ApiErrorCodes.ErrBadRequest,
                    errorUniqueId: 'aa3caf5fca8f4742b138fbddbd06f464',
                    message: null,
                    timeStamp: date.toISOString(),
                    validationErrors: null
                };
                break;

            case 'EAIA':
                // Application Key was not found in the database
                statusMessage = {
                    code: '401',
                    descrip: 'Unauthorized',
                    errorCode: <any>ApiErrorCodes.ErrAuthInvalidAppKey,
                    errorUniqueId: 'aa3caf5fca8f4742b138fbddbd06f465',
                    message: null,
                    timeStamp: date.toISOString(),
                    validationErrors: null
                };
                break;

            case 'EAIC':
                // Invalid credentials.
                statusMessage = {
                    code: '401',
                    descrip: 'Unauthorized',
                    errorCode: <any>ApiErrorCodes.ErrAuthInvalidCredentials,
                    errorUniqueId: 'aa3caf5fca8f4742b138fbddbd06f466',
                    message: null,
                    timeStamp: date.toISOString(),
                    validationErrors: null
                };
                break;

            case 'EAIAC':
                // Cuenta de Empresa o Pasajero inactiva. 
                statusMessage = {
                    code: '401',
                    descrip: 'Unauthorized',
                    errorCode: <any>ApiErrorCodes.ErrAuthInanctiveAccount,
                    errorUniqueId: 'aa3caf5fca8f4742b138fbddbd06f467',
                    message: null,
                    timeStamp: date.toISOString(),
                    validationErrors: null
                };
                break;

            case 'EAN':
                // Pasajero sin privilegio de login.
                statusMessage = {
                    code: '401',
                    descrip: 'Unauthorized',
                    errorCode: <any>ApiErrorCodes.ErrAuthNoLoginPrivilege,
                    errorUniqueId: 'aa3caf5fca8f4742b138fbddbd06f468',
                    message: null,
                    timeStamp: date.toISOString(),
                    validationErrors: null
                };
                break;

            case 'EAUGT':
                // Only grant_type=password is accepted by this server.
                statusMessage = {
                    code: '403',
                    descrip: 'Forbidden',
                    errorCode: <any>ApiErrorCodes.ErrAuthUnsupportedGrantType,
                    errorUniqueId: 'aa3caf5fca8f4742b138fbddbd06f469',
                    message: null,
                    timeStamp: date.toISOString(),
                    validationErrors: null
                };
                break;

            case '500':
                // Server encountered and internal error while attempting to process the request.
                statusMessage = {
                    code: '500',
                    descrip: 'InternalServerError',
                    errorCode: <any>ApiErrorCodes.ErrInternalServerError,
                    errorUniqueId: 'aa3caf5fca8f4742b138fbddbd06f464',
                    message: null,
                    timeStamp: date.toISOString(),
                    validationErrors: null
                };
                break;

            case '502':
                // No response from external API.
                statusMessage = {
                    code: '502',
                    descrip: 'BadGateway',
                    errorCode: <any>ApiErrorCodes.ErrNoResponseFromApi,
                    errorUniqueId: 'aa3caf5fca8f4742b138fbddbd06f464',
                    message: null,
                    timeStamp: date.toISOString(),
                    validationErrors: null
                };
                break;

            default:
                jwtSuccess = {
                    access_token: 'eyJhbGciOiJSUzI1NiIsImtpZCI6Ik9QS1JFOF9NRUVJMVBQWUNYX1JXX0ZGMDUzVEZDUUdEREFRUzVVM0QiLCJ0eXAiOiJKV1QifQ.eyJwYXhfZnVsbG5hbWUiOiJTb21tYSwgRXN0ZWJhbiBIdWdvIiwiZXh0ZXJuYWxfdG9rZW4iOiIwZThjNWZjYjY3MDY0ZDZkYjQwMjc4YThmMzNhZWNmZSIsImNvbXBhbnlfbmFtZSI6IkNMSUVOVEUgREUgUFJVRUJBICoqKioqKioqKioqKioqIiwianRpIjoiNjRmNThmOTAtNDc1OC00MGY3LThmMTgtMDE1OTM1ZmQ1NjBiIiwidXNhZ2UiOiJhY2Nlc3NfdG9rZW4iLCJjZmRfbHZsIjoicHJpdmF0ZSIsInNjb3BlIjpbIm9wZW5pZCIsImVtYWlsIiwicHJvZmlsZSJdLCJzdWIiOiJlaHNAc29tbXl0ZWNoLmNvbS5hciIsImF1ZCI6Inp3aXRjaGVyLmNvbSIsImF6cCI6ImFwcGtleTEiLCJuYmYiOjE1MDEwNzg3MTksImV4cCI6MTUwMTEyMTkxNiwiaWF0IjoxNTAxMDc4NzE5LCJpc3MiOiJodHRwOi8vbG9jYWxob3N0OjUwMDAvIn0.Mr1I3BmyTSBDWbrCZE15HQg1EWerWpq7WdUM2CNiG8wxbLQLtLXOETMx_pL36ZpVpvcK_qAb1nZeqJzPOWzR5yEV-Vkj8dCIyYIvgRFCRro7YiUBj3FlXfOityh2sUBMjE4OTWx9yfWhzThFTQ4wei7upRbUuBytKB4hNo1PRwwf9AJ9Bymhi-AR9aWYo1_doieTOj3CsjUicVzFSdV3DdTUNmWwpqHXbjI0hn_PrZILGbuuR_LZJ0Zz5fW40_vjdjy0obmmHI50rrKXV-EI8DdaakKRxaR12Qc_wPu3ha6RVBh0W_WPEteJQBLKyvIeoWTFOHWKB8puWAtZzKiPKw',
                    expires_in: 43197,
                    id_token: 'eyJhbGciOiJSUzI1NiIsImtpZCI6Ik9QS1JFOF9NRUVJMVBQWUNYX1JXX0ZGMDUzVEZDUUdEREFRUzVVM0QiLCJ0eXAiOiJKV1QifQ.eyJzdWIiOiJlaHNAc29tbXl0ZWNoLmNvbS5hciIsImp0aSI6IjdkNTJkNDMxLTQ5Y2ItNDBlZi04YTk1LWM2YzM4ZjJhM2Y4YiIsInVzYWdlIjoiaWRlbnRpdHlfdG9rZW4iLCJjZmRfbHZsIjoicHJpdmF0ZSIsImF1ZCI6ImFwcGtleTEiLCJhdF9oYXNoIjoiVHIyM3JNSE80Y1MxLWQyOGpJX0xtQSIsImF6cCI6ImFwcGtleTEiLCJuYmYiOjE1MDEwNzg3MTksImV4cCI6MTUwMTA3OTkxOSwiaWF0IjoxNTAxMDc4NzE5LCJpc3MiOiJodHRwOi8vbG9jYWxob3N0OjUwMDAvIn0.c-Bz5ByQGv5AF3KEstWIG_yyQTj-RDQ2CiusD-67Qeb2HJD5ZhYwnYt7Dqd-ChhLpf2JtWqf1BlVONDpnygcZC5BZHrepZFx__yVVO7KOvro590AMPxSkRmAAu0OyCpqtfSUweGi4KGVSuFne78neUHcVeYnR9dLedRVORfHYNGlJYMdNmkXdiXiqPfkxdCiy_fDg8EmORxKvuX2VU4l0gcQhUMcUKBcTtQmSxaPPPPgeNhzhCFoNj3sWP3k734N6qZDwDEYdxrTuwjEZ_C0d9zen0qFMb9EeuPBzGAdSRaKh8s9qYgwVPOc2HlCSxESR2pI6TKDd0YrCHjvvksB1w',
                    token_type: 'Bearer'
                };
                break;
        }

        return statusMessage ? [statusMessage.code, statusMessage, {}] : this.ok(jwtSuccess);
    }

    public postPasswordResetRequest(method: string, url: string, data: any, headers: any): any[] {
        this.$log.debug(`${AuthMock.name}::postAuthLogin method %o, url %o, data %o, headers %o`, method, url, data, headers);

        let statusMessage: IStatusMessage;
        const date: Date = new Date();

        const re: RegExp = new RegExp('/auth/(.*)/passwordresetrequest$', 'i');
        const match: RegExpExecArray = re.exec(url);
        const userName: string = match[1];
        switch (userName) {

            case '400':
                // Requested invalid or incomplete.
                statusMessage = {
                    code: '400',
                    descrip: 'BadRequest',
                    errorCode: <any>ApiErrorCodes.ErrBadRequest,
                    errorUniqueId: 'f547b3782a8547f8a5dd8de227ebb842',
                    message: null,
                    timeStamp: date.toISOString(),
                    validationErrors: null
                };
                break;

            case '404':
                // UserName not found.
                statusMessage = {
                    code: '404',
                    descrip: 'NotFound',
                    errorUniqueId: 'f547b3782a8547f8a5dd8de227ebb843',
                    errorCode: 'ERR',
                    timeStamp: date.toISOString(),
                    message: null,
                    validationErrors: null
                };
                break;

            case '500':
                // Server encountered and internal error while attempting to process the request.
                statusMessage = {
                    code: '500',
                    descrip: 'InternalServerError',
                    errorCode: <any>ApiErrorCodes.ErrInternalServerError,
                    errorUniqueId: 'f547b3782a8547f8a5dd8de227ebb844',
                    message: null,
                    timeStamp: date.toISOString(),
                    validationErrors: null
                };
                break;

            case '502':
                // No response from external API.
                statusMessage = {
                    code: '502',
                    descrip: 'BadGateway',
                    errorCode: <any>ApiErrorCodes.ErrNoResponseFromApi,
                    errorUniqueId: 'f547b3782a8547f8a5dd8de227ebb845',
                    message: null,
                    timeStamp: date.toISOString(),
                    validationErrors: null
                };
                break;

            default:
                break;


        }
        return statusMessage ? [statusMessage.code, statusMessage, {}] : this.noContent();
    }


    /**
     * Devuevlve un mock para request de PUT /auth/password.
     * @param {string} method Nombre de verbo http.
     * @param {string} url URL completa del endpoint.
     * @param {*} data Objeto que se envía en el campo data del request (body).
     * @param {*} headers Objeto que se envía en el campo headers del request.
     * @returns {any[]} HTTP Response.
     * @memberof AuthMock
     */
    // tslint:disable-next-line:max-func-body-length
    public putChangePasswordRequest(method: string, url: string, data: any, headers: any): any[] {
        this.$log.debug(`${AuthMock.name}::putChangePasswordRequest method %o, url %o, data %o, headers %o`, method, url, data, headers);

        const payload: IChangePasswordRequest = this.getData<IChangePasswordRequest>(data);
        const date: Date = new Date();
        let statusMessage: IStatusMessage;


        switch (payload.oldPassword) {

            case 'EANNC':
                // Mock para confirmación de contraseña inválida.
                statusMessage = {
                    code: '400',
                    descrip: 'BadRequest',
                    errorCode: <any>ApiErrorCodes.ErrAuthNewPasswordNotmatchConfirmation,
                    errorUniqueId: 'aa3caf5fca8f4742b138fbddbd06f464',
                    message: null,
                    timeStamp: date.toISOString(),
                    validationErrors: null
                };
                break;

            case 'EANT':
                // Mock para password insegura.
                statusMessage = {
                    code: '400',
                    descrip: 'BadRequest',
                    errorCode: <any>ApiErrorCodes.ErrAuthNewPasswordTooweak,
                    errorUniqueId: 'aa3caf5fca8f4742b138fbddbd06f465',
                    message: null,
                    timeStamp: date.toISOString(),
                    validationErrors: null
                };
                break;

            case 'EAA':
                // Mock para header inválido.
                statusMessage = {
                    code: '401',
                    descrip: 'Unauthorized',
                    errorCode: <any>ApiErrorCodes.ErrAuthAuthorizationHeader,
                    errorUniqueId: 'aa3caf5fca8f4742b138fbddbd06f466',
                    message: null,
                    timeStamp: date.toISOString(),
                    validationErrors: null
                };
                break;

            case 'EAAI':
                // Mock para token inválido.
                statusMessage = {
                    code: '401',
                    descrip: 'Unauthorized',
                    errorCode: <any>ApiErrorCodes.ErrAuthAccessTokenInvalid,
                    errorUniqueId: 'aa3caf5fca8f4742b138fbddbd06f467',
                    message: null,
                    timeStamp: date.toISOString(),
                    validationErrors: null
                };
                break;

            case 'EAAE':
                // Mock para token expirado.
                statusMessage = {
                    code: '401',
                    descrip: 'Unauthorized',
                    errorCode: <any>ApiErrorCodes.ErrAuthAccessTokenExpired,
                    errorUniqueId: 'aa3caf5fca8f4742b138fbddbd06f468',
                    message: null,
                    timeStamp: date.toISOString(),
                    validationErrors: null
                };
                break;

            case 'EAAO':
                // Mock para límite de intentos.
                statusMessage = {
                    code: '401',
                    descrip: 'Unauthorized',
                    errorCode: <any>ApiErrorCodes.ErrAuthAccessTokenOverHitsLimit,
                    errorUniqueId: 'aa3caf5fca8f4742b138fbddbd06f469',
                    message: null,
                    timeStamp: date.toISOString(),
                    validationErrors: null
                };
                break;

            case 'EAI':
                // Mock para credenciales inválidas.
                statusMessage = {
                    code: '401',
                    descrip: 'Unauthorized',
                    errorCode: <any>ApiErrorCodes.ErrAuthInvalidCredentials,
                    errorUniqueId: 'aa3caf5fca8f4742b138fbddbd06f470',
                    message: null,
                    timeStamp: date.toISOString(),
                    validationErrors: null
                };
                break;

            case '500':
                // Mock para error genérico.
                statusMessage = {
                    code: '500',
                    descrip: 'InternalServerError',
                    errorCode: <any>ApiErrorCodes.ErrInternalServerError,
                    errorUniqueId: 'aa3caf5fca8f4742b138fbddbd06f471',
                    message: null,
                    timeStamp: date.toISOString(),
                    validationErrors: null
                };
                break;

            case '502':
                // Mock para No response from external API.
                statusMessage = {
                    code: '502',
                    descrip: 'BadGateway',
                    errorCode: <any>ApiErrorCodes.ErrNoResponseFromApi,
                    errorUniqueId: 'aa3caf5fca8f4742b138fbddbd06f472',
                    message: null,
                    timeStamp: date.toISOString(),
                    validationErrors: null
                };
                break;

            default:
                break;
        }

        return statusMessage ? [statusMessage.code, statusMessage, {}] : this.noContent();
    }

    public getSearchClients(method: string, url: string, data: any, headers: any): any[] {
        this.$log.debug(`${AuthMock.name}::getSearchClients method %o, url %o, data %o, headers %o`, method, url, data, headers);
        const re: RegExp = new RegExp('/auth/clients/(.*)', 'gi');
        const match: RegExpExecArray = re.exec(url);
        const searchText: string = match[1];
        return this.ok(this.clientResponse.filter((name: string) => {return name.toLowerCase().indexOf(searchText.toLowerCase()) > -1;}));
    }

    private clientResponse: string[] = ['Fabian Soligo',
        'Jesus Suarez',
        'Sebastian Bricchi',
        'Esteban Somma',
        'Damian Eiff',
        'Nestor Jacque',
        'Ricardo Espindola'
    ];

}