'use strict';

import { NgModule, ModuleDecorated } from 'angular-ts-decorators';
import { ModuleHelper } from '../../../core/module.helper';
import { ConfigService } from '../../../app/app-config.service';
import { CoreModule } from '../../../core/core.module';
import { AuthMock } from './auth-mock';
import { Environments } from '../../../core/entities/environments';

/**
 * Declara el módulo para Auth.
 * @export
 * @class AuthApiModule
 * @implements {ModuleDecorated}
 */
@NgModule({
    declarations: [],
    providers: [],
    imports: [
        'ngMockE2E'
    ]
})
export class AuthMockModule implements ModuleDecorated {

    /**
     * Configuración del módulo.
     */
    public config(): void {
        'ngInject';
        const $log: angular.ILogService = ModuleHelper.getLogger();
        $log.debug(`${AuthMockModule.name}::config`);
    }

    /**
     * Corre el módulo.
     * @param {angular.IHttpBackendService} $httpBackend Servicio para configurar endpoints y mockear su response.
     * @param {ConfigService} ConfigService * Settings generales de la app.
     */
    public run($httpBackend: angular.IHttpBackendService,
               // tslint:disable-next-line:variable-name no-shadowed-variable Se deshabilita ya que solo muestra  warning en el pipeline de bitbucket
               ConfigService: ConfigService): void {
        'ngInject';
        const $log: angular.ILogService = ModuleHelper.getLogger();
        $log.debug(`${AuthMockModule.name}::config`);

        const authMock: AuthMock = new AuthMock($log);
        const urlBase: string = 'https://auth.zwitcher.com';
        const resetPasswordUrl: string = `${urlBase}/auth/resetpassword`;
        const loginUrl: string = `${urlBase}/connect/token`;
        const passwordResetRequestRegex: RegExp = new RegExp(`${urlBase}/auth/(.*)/passwordresetrequest`, 'i');
        const searchClientsRegex: RegExp = new RegExp(`${urlBase}/auth/clients/(.*)`, 'i');
        const changePasswordUrl: string = `${urlBase}/auth/password`;
        const env: string = ConfigService.getEnvironment();

        // tslint:disable-next-line:no-any
        if (env === <any>Environments.Mock) {
            // Request resetPassword.
            $httpBackend.whenPOST(resetPasswordUrl)
                .respond(authMock.postAuthResetPassword.bind(authMock));

            // Request login.
            $httpBackend.whenPOST(loginUrl)
                .respond(authMock.postAuthLogin.bind(authMock));

            // Request passwordResetRequest.
            $httpBackend.whenPOST(passwordResetRequestRegex)
                .respond(authMock.postPasswordResetRequest.bind(authMock));

            // Request changePassword.
            $httpBackend.whenPUT(changePasswordUrl)
                .respond(authMock.putChangePasswordRequest.bind(authMock));

            $httpBackend.whenGET(searchClientsRegex)
                .respond(authMock.getSearchClients.bind(authMock));

        } else {

            $httpBackend.whenPOST(resetPasswordUrl).passThrough();
            $httpBackend.whenPOST(loginUrl).passThrough();
            $httpBackend.whenPOST(passwordResetRequestRegex).passThrough();
            $httpBackend.whenPUT(changePasswordUrl).passThrough();

        }
    }
}